<?php

/**
 * <b>Read.class: </b> [ LEITURA ]
 * Realiza a leitura do banco de dados de forma genérica
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Read extends Conn {

	private $select;
	private $places;
	private $result;

	/** @var PDOStatement */
	private $read;

	/** @var PDO */
	private $conn;

	/**
	 * <b>exeRead:</b> Executa uma consulta  simplificada no banco de dados utilizando Prepared Statements.
	 * Basta informar a tabela,os termos e os parâmetros ,separados por &.
	 * 
	 * @param STRING $tabela = Informe o nome da tabela no banco!;
	 * @param STRING $termos = Informe os termos da cláusula;
	 * @param STRING $parseString = Informe os parâmetros da cláusula separados por '&'(parametro=valor&parametro2=valor2);
	 */
	public function exeRead($tabela, $termos = null, $parseString = null) {
		if (!empty($parseString)) {
			parse_str($parseString, $this->places);
		}
		$this->select = "SELECT * FROM {$tabela} {$termos}";
		$this->Execute();
	}

	/**
	 * Retorna o resultado da consulta.
	 * 
	 * @return ARRAY -> Se retornar resultados
	 * @return NULL  -> Se não conseguir executar a consulta
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * Essa classe retorna o número de resultados da consulta.
	 * 
	 * 
	 * @return INT = Retorna o número de resultados
	 */
	public function getRowCount() {
		return $this->read->rowCount();
	}

	/**
	 * <b>fullRead:</b> Método criado para executar cláusulas mais complexas, para usar
	 * informe completamente os comandos.
	 * 
	 * @param STRING $query = Cláusula de s consulta completa
	 * @param STRING $parseString = Parâmetros da query
	 */
	public function fullRead($query, $parseString = NULL) {
		$this->select = (string) $query;

		if (!empty($parseString)) {
			parse_str($parseString, $this->places);
		}
		$this->Execute();
	}

	/**
	 * Método para alterar os valores dos parâmetros da consulta
	 * 
	 * @param STRING $parseString = Parâmetros da consulta
	 */
	public function setPlaces($parseString) {
		parse_str($parseString, $this->places);
		$this->Execute();
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	//
	//Obtém o PDO e prepara a query
	private function connect() {
		$this->conn = parent::getConn();
		$this->read = $this->conn->prepare($this->select);
		$this->read->setFetchMode(PDO::FETCH_ASSOC);
	}

	//cria a sintaxe da query para Prepared Statements
	private function getSyntax() {
		if (isset($this->places)) {
			foreach ($this->places as $vinculo => $valor) {
				//palavras reservadas, para passar somente int na query
				if ($vinculo == 'limit' || $vinculo == 'offset') {
					$valor = (int) $valor;
				}

				$this->read->bindValue(":{$vinculo}", $valor, (is_int($valor) ? PDO::PARAM_INT : PDO::PARAM_STR));
			}
		}
	}

	//obtem a conexão e a syntax, executa a query.
	//letra maiuscula para não confundir com o execute() do PDO
	private function Execute() {
		$this->connect();
		try {
			$this->getSyntax();
			$this->read->execute();
			$this->result = $this->read->fetchAll();
		} catch (PDOException $e) {
			$this->result = null;
			WSErro("Erro ao realizar leitura: {$e->getMessage()}", $e->getCode());
		}
	}

}
