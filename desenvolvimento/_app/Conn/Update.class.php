<?php

/**
 * <b>Update.class: </b> [ UPDATE ]
 * Classe responsável por atualizações genéricas no banco de dados
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Update extends Conn {

	private $tabela;
	private $dados;
	private $termos;
	private $places;
	private $result;

	/** @var PDOStatement */
	private $update;

	/** @var PDO */
	private $conn;

	/**
	 * <b>exeUpdate: </b> Executa uma atualização genérica no banco de dados, informe o nome da tabela, os dados em array, os termos e os parâmetros.
	 * 
	 * @param STRING $tabela = Nome da tabela a ser atualizada
	 * @param ARRAY  $dados  = nome do campo e o valor ('nome_do_campo' => 'valor')
	 * @param STRING $termos = Termos da procedure
	 * @param STRING $parseString = valor dos parâmetros separados por & ('parametro=valor&param2=valor2');
	 */
	public function exeUpdate($tabela, array $dados, $termos, $parseString) {
		$this->tabela = (string) $tabela;
		$this->dados = $dados;
		$this->termos = (string) $termos;

		parse_str($parseString, $this->places);
		$this->getSyntax();
		$this->Execute();
	}

	/**
	 * @return BOOLEAN = Retorna true ou false, informando se o update foi realizado.
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * @return INT = retorna o número de linhas que foram atualizadas com sucesso.
	 */
	public function getRowCount() {
		return $this->update->rowCount();
	}

	/**
	 * <b>setPlaces: </b> Este método permite realizar diversas operações com a mesma procedure, apenas trocando o valor dos parâmetros.
	 * 
	 * @param STRING $parseString = valor dos parâmetros separados por & ('parametro=valor&param2=valor2');
	 */
	public function setPlaces($parseString) {
		parse_str($parseString, $this->places);
		$this->getSyntax();
		$this->Execute();
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
//
	//realiza a conexão com o banco e prepara a query.
	private function connect() {
		$this->conn = parent::getConn();
		$this->update = $this->conn->prepare($this->update);
	}

	//organiza a query, setando os campos e os valores e os termos.
	private function getSyntax() {
		foreach ($this->dados as $key => $value) {
			$places[] = $key . ' = :' . $key;
		}

		$places = implode(", ", $places);
		$this->update = "UPDATE {$this->tabela} SET {$places} {$this->termos}";
	}

	//executa o update, ou retorna uma exception.
	private function Execute() {
		$this->connect();
		try {
			$this->update->execute(array_merge($this->dados, $this->places));
			$this->result = true;
		} catch (PDOException $e) {
			$this->result = null;
			WSErro("Erro ao realizar leitura: {$e->getMessage()}", $e->getCode());
		}
	}

}
