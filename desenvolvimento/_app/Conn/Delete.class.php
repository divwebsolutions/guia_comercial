<?php

/**
 * <b>Update.class: </b> [ UPDATE ]
 * Classe responsável por deletar genéricamente no banco de dados
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Delete extends Conn {

	private $tabela;
	private $termos;
	private $places;
	private $result;

	/** @var PDOStatement */
	private $delete;

	/** @var PDO */
	private $conn;

	/**
	 * Executa o delete, requisitando o nome da tabela, os termos e os parâmetros.
	 * 
	 * @param String $tabela
	 * @param String $termos
	 * @param String $parseString
	 */
	public function exeDelete($tabela, $termos, $parseString) {
		$this->tabela = (string) $tabela;
		$this->termos = (string) $termos;

		parse_str($parseString, $this->places);
		$this->getSyntax();
		$this->Execute();
	}

	/**
	 * @return BOOLEAN = Retorna true ou false, informando se o delete foi realizado.
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * @return INT = retorna o número de linhas que foram atualizadas com sucesso.
	 */
	public function getRowCount() {
		return $this->delete->rowCount();
	}

	/**
	 * <b>setPlaces: </b> Este método permite realizar diversas operações com a mesma procedure, apenas trocando o valor dos parâmetros.
	 * 
	 * @param STRING $parseString = valor dos parâmetros separados por & ('parametro=valor&param2=valor2');
	 */
	public function setPlaces($parseString) {
		parse_str($parseString, $this->places);
		$this->getSyntax();
		$this->Execute();
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
//
	//realiza a conexão com o banco e prepara a query.
	private function connect() {
		$this->conn = parent::getConn();
		$this->delete = $this->conn->prepare($this->delete);
	}

	//organiza a query, setando os campos e os valores e os termos.
	private function getSyntax() {
		$this->delete = "DELETE FROM {$this->tabela} {$this->termos}";
	}

	//executa o delete, ou retorna uma exception.
	private function Execute() {
		$this->connect();
		try {
			$this->delete->execute($this->places);
			$this->result = true;
		} catch (PDOException $e) {
			$this->result = null;
			WSErro("Erro ao realizar cadastro: {$e->getMessage()}", $e->getCode());
		}
	}

}
