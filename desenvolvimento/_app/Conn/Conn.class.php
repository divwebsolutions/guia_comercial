<?php

/**
 * Conn.class [ CONEXÃO ]
 * Classe abstrata de conexão. Padrão singleTon.
 * Retorna um objeto PDO pelo método estático getConn();
 *  
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Conn {

	private static $host = HOST;
	private static $user = USER;
	private static $pass = PASS;
	private static $dbsa = DBSA;

	/** @var PDO */
	private static $connect = null;

	/** Conecta com o banco de dados com o pattern singleton.
	 * Retorna um objeto PDO! 	 
	 */
	private static function conectar() {
		try {
			if (self::$connect == null) {
				$dsn = 'mysql:host=' . self::$host . ';dbname=' . self::$dbsa;
				$options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'];
				self::$connect = new PDO($dsn, self::$user, self::$pass, $options);
			}
		} catch (PDOException $e) {
			PHPErro($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
			die;
		}

		self::$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return self::$connect;
	}

	/** Retorna um objeto PDO Singleton pattern. */
	public static function getConn() {
		return self::conectar();
	}

}
