<?php

/**
 * <b>Create.class:</b> [ CADASTRO ]
 * Classe responsável pela criação de cadastros genéricos no banco de dados 
 * de forma dinâmica.
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Create extends Conn {

	private $tabela;
	private $dados;
	private $result;

	/** @var PDOStatement */
	private $create;

	/** @var PDO */
	private $conn;

	/**
	 * <b>exeCreate: </b> Executa um cadastro simplificado no banco, utilizando prepared statements.<br/>
	 * Basta informar o nome da tabela a ser inserida e um array atribuitivo com o nome da coluna e valor.
	 * 
	 * @param STRING $tabela => Informe o nome da tabela a qual vai ser inserido o cadastro
	 * @param ARRAY $dados => Informe um array atribuitivo com os dados. (Nome Da Coluna => Valor)
	 */
	public function exeCreate($tabela, array $dados) {
		$this->tabela = (string) $tabela;
		$this->dados = $dados;
		$this->getSyntax();
		$this->Execute();
	}
	
	/**
	 * <b>getResult: </b> Retorna o ID do último cadastro, ou retorna null se obtiver erro.
	 * @return INT/NULL 
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	//
	//Conecta com o banco e prepara a query
	private function connect() {
		$this->conn = parent::getConn();
		$this->create = $this->conn->prepare($this->create);
	}
	
	//Monta a query
	private function getSyntax() {
		$fields = implode(', ', array_keys($this->dados));
		$places = ':' . implode(', :', array_keys($this->dados));
		$this->create = "INSERT INTO {$this->tabela} ({$fields}) VALUES ({$places})";
	}

	//letra maiuscula para não confundir com o execute do PDO.
	//executa a query
	private function Execute() {
		$this->connect();
		try {
			$this->create->execute($this->dados);
			$this->result = $this->conn->lastInsertId();
		} catch (PDOException $e) {
			$this->result = null;
			WSErro("Erro ao realizar cadastro: {$e->getMessage()}", $e->getCode());
		}
	}

}
