<?php
date_default_timezone_set("America/Sao_Paulo");

// CONFIGURAÇÕES DO BANCO ####################
define('HOST', '');//servidor
define('USER', '');//usuário
define('PASS', '');//senha
define('DBSA', '');//banco de dados

// DEFINE SERVIDOR DE E-MAIL #################
define('MAILUSER', '');//usuário de email para disparar
define('MAILPASS', '');//senha do email
define('MAILPORT', '');//porta smtp
define('MAILHOST', '');//host smtp

// DEFINE A HOME DO SITE #####################
define('HOME', '');//endereço base do site (nunca deixar a barra no final)
define('THEME', 'cidadeonline');//nome do tema

// DEFINE IDENTIDADE DO SITE #################
define('SITENAME', 'Cidade Online');
define('SITEDESC', 'Este site foi desenvolvido no curso de PHP Orientado a Objetos da UPINSIDE TREINAMENTOS. O mesmo utiliza a arquitetura mvc');
define('INCLUDE_PATH', HOME . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR . THEME);
define('REQUIRE_PATH', 'themes' . DIRECTORY_SEPARATOR . THEME);

// AUTOLOAD DE CLASSES #######################
function __autoload($Class) {
	$cDir = ['Conn', 'Helpers', 'Models', 'Library'];
	$iDir = null;

	foreach ($cDir as $dirname) {
		if (!$iDir && file_exists(__DIR__ . DIRECTORY_SEPARATOR . $dirname . DIRECTORY_SEPARATOR . $Class . '.class.php') && !is_dir(__DIR__ . DIRECTORY_SEPARATOR . $dirname . DIRECTORY_SEPARATOR . $Class . '.class.php')) {
			include_once (__DIR__ . DIRECTORY_SEPARATOR . $dirname . DIRECTORY_SEPARATOR . $Class . '.class.php');
			$iDir = TRUE;
		}
	}

	if (!$iDir) {
		trigger_error("Não foi possivel incluir {$Class}.class.php", E_USER_ERROR);
		die;
	}
}

//  TRATAMENTO DE ERROS  ####################
//  CSS Constantes :: Mensagens de Erro
define('WS_ACCEPT', 'accept');
define('WS_INFOR', 'infor');
define('WS_ALERT', 'alert');
define('WS_ERROR', 'error');

//  WSErro :: Exibe erros lançados :: Front
function WSErro($errMsg, $errNo, $errDie = null) {
	$cssClass = ($errNo == E_USER_NOTICE ? WS_INFOR : ($errNo == E_USER_WARNING ? WS_ALERT : ($errNo == E_USER_ERROR ? WS_ERROR : $errNo)));
	echo "<p class = \"trigger {$cssClass}\">{$errMsg}<span class=\"ajax_close\"></span></p>";

//Se a variavel estiver setada, matar a aplicação.
	if ($errDie) {
		die;
	}
}

// PHPErro :: personaliza o gatilho do PHP
function PHPErro($errNo, $errMsg, $errFile, $errLine) {
	$cssClass = ($errNo == E_USER_NOTICE ? WS_INFOR : ($errNo == E_USER_WARNING ? WS_ALERT : ($errNo == E_USER_ERROR ? WS_ERROR : $errNo)));
	echo "<p class=\"trigger {$cssClass}\">";
	echo "<b>Erro na linha: {$errLine} ::</b> {$errMsg} <br>";
	echo "<small>{$errFile }</small>";
	echo "<span class=\"ajax_close\"></span></p>";

	if ($errNo == E_USER_ERROR) {
		die;
	}
}

set_error_handler('PHPErro');
