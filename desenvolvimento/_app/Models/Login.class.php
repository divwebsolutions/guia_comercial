<?php

/**
 * Login.class [ MODEL ]
 * Classe responsavel por autenticar, validar e checar usuários do sistema de login
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Login {

	private $level;
	private $email;
	private $senha;
	private $error;
	private $result;

	/**
	 * <b>Informar Level: </b> Informe o nível de acesso mínimo para a área a ser protegida.
	 * 
	 * @param INT $level = Nível minimo para acesso.
	 */
	function __construct($level) {
		$this->level = (int) $level;
	}

	/**
	 * <b>exeLogin: </b>método responsável por executar o login no painel de admin
	 * @param ARRAY $userData = Dados do usuário recuperados através do formulário de login 
	 */
	public function exeLogin(array $userData) {
		$this->email = (string) strip_tags(trim($userData['user']));
		$this->senha = (string) strip_tags(trim($userData['pass']));
		$this->setLogin();
	}

	/**
	 * 
	 * @return ARRAY = Obtém dados do usuário
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * 
	 * @return ARRAY = Obtém mensagens do sistema.
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 * <b>Check Login: </b> Execute esse método para verificar a sessao USERLOGIN e revalidar o acesso
	 * para proteger telas restritivas.
	 * 
	 * @return BOOLEAN $login = Retorna true ou mata a sessão e retorna false.
	 */
	public function checkLogin() {
		if (empty($_SESSION['userLogin']) || $_SESSION['userLogin']['user_level'] < $this->level) {
			unset($_SESSION['userLogin']);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	//Verifica a autenticidade dos dados informados e executa caso não encontre erros.
	private function setLogin() {
		if (!$this->email || !$this->senha || !Check::validaEmail($this->email)) {
			$this->error = ['Informe seu E-mail e Senha para efetuar o login', WS_INFOR];
			$this->result = false;
		} elseif (!$this->getUser()) {
			$this->error = ['E-mail ou Senha incorretos!', WS_ALERT];
			$this->result = false;
		} elseif ($this->result['user_level'] < $this->level) {
			$this->error = ["Desculpe {$this->result['user_name']}, você não tem permissão para acessar esta área!", WS_ERROR];
			$this->result = false;
		} else {
			$this->Execute();
		}
	}

	//Verifica se o usuário está registrado no banco de dados.
	private function getUser() {
		$this->senha = md5($this->senha);
		$read = new Read;
		$read->exeRead('ws_users', 'WHERE user_email = :e AND user_password = :p', "e={$this->email}&p={$this->senha}");
		if ($read->getResult()) {
			$this->result = $read->getResult()[0];
			return true;
		} else {
			return false;
		}
	}

	//Verifica e inicia a sessão do usuário.
	private function Execute() {
		if (!session_id()) {
			session_start();
		}

		$_SESSION['userLogin'] = $this->result;
		$_SERVER['userLogin'] = $_SESSION['userLogin'];
		$this->error = ["Olá {$this->result['user_name']}, seja bem vindo(a). Aguarde redirecionamento"];
		$this->result = true;
	}

}
