<?php

/**
 * Seo [ MODEL ]
 * Classe de apoio para o modelo LINK. Pode ser utilizada para gerar SSEO(Search and social Engine Optimization) para as páginas do sistema!
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Seo {

	private $file;
	private $link;
	private $data;
	private $tags;

	/* DADOS POVOADOS */
	private $seoTags;
	private $seoData;

	function __construct($file, $link) {
		$this->file = strip_tags(trim($file));
		$this->link = strip_tags(trim($link));
	}

	public function getTags() {
		$this->checkData();
		return $this->seoTags;
	}

	public function getData() {
		$this->checkData();
		return $this->seoData;
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	private function checkData() {
		if (!$this->seoData) {
			$this->getSeo();
		}
	}

	private function getSeo() {
		$readSeo = new Read;

		switch ($this->file) {
			//SEO:: POST
			case 'artigo':
				$admin = (isset($_SESSION['userLogin']['user_level']) && $_SESSION['userLogin']['user_level'] == 3 ? true : false);
				$check = ($admin ? '' : 'post_status = 1 AND');

				$readSeo->exeRead('ws_posts', "WHERE {$check} post_name = :link", "link={$this->link}");
				if (!$readSeo->getResult()) {
					$this->seoData = null;
					$this->seoTags = null;
				} else {
					extract($readSeo->getResult()[0]);
					$this->seoData = $readSeo->getResult()[0];
					$this->data = [$post_title . ' - ' . SITENAME, $post_content, HOME . "/artigo/{$post_name}", HOME . "/uploads/{$post_cover}"];

					//POST:: post_views
					$arrUpdate = ['post_views' => $post_views + 1];
					$update = new Update;
					$update->exeUpdate('ws_posts', $arrUpdate, 'WHERE post_id = :postid', "postid={$post_id}");
				}
				break;

			//SEO:: CATEGORIA
			case 'categoria':
				$readSeo->exeRead('ws_categories', "WHERE category_name = :link", "link={$this->link}");
				if (!$readSeo->getResult()) {
					$this->seoData = null;
					$this->seoTags = null;
				} else {
					extract($readSeo->getResult()[0]);
					$this->seoData = $readSeo->getResult()[0];
					$this->data = [$category_title . ' - ' . SITENAME, $category_content, HOME . "/categoria/{$category_name}", INCLUDE_PATH . '/images/site.png'];

					//CATEGORY:: conta views da categoria
					$arrUpdate = ['category_views' => $category_views + 1];
					$update = new Update;
					$update->exeUpdate('ws_categories', $arrUpdate, 'WHERE category_id = :catid', "catid={$category_id}");
				}
				break;

			//SEO:: PESQUISA
			case 'pesquisa':
				$readSeo->exeRead('ws_posts', "WHERE post_status = 1 AND (post_title LIKE '%' :link '%' OR post_content LIKE '%' :link '%')", "link={$this->link}");
				if (!$readSeo->getResult()) {
					$this->seoData = null;
					$this->seoTags = null;
				} else {
					$this->seoData['count'] = $readSeo->getRowCount();
					$this->data = ["Pesquisa por: {$this->link}" . ' - ' . SITENAME, "Sua pesquisa por {$this->link} retornou {$this->seoData['count']} resultados!", HOME . "/pesquisa/{$this->link}", INCLUDE_PATH . '/images/site.png'];
				}
				break;

			//SEO:: LISTA EMPRESAS
			case 'empresas':
				$name = ucwords(str_replace("-", " ", $this->link));
				$this->seoData = ["empresa_link" => $this->link, "empresa_cat" => $name];
				$this->data = ["Empresas {$name} - " . SITENAME, "Confira o guia completo de sua cidade, e encontre empresas {$this->link}.", HOME . '/empresas/' . $this->link, INCLUDE_PATH . '/images/site.png'];
				break;

			//SEO:: EMPRESA SINGLE
			case 'empresa':
				$admin = (isset($_SESSION['userLogin']['user_level']) && $_SESSION['userLogin']['user_level'] == 3 ? true : false);
				$check = ($admin ? '' : 'empresa_status = 1 AND');

				$readSeo->exeRead('app_empresas', "WHERE {$check} empresa_name = :link", "link={$this->link}");
				if (!$readSeo->getResult()) {
					$this->seoData = null;
					$this->seoTags = null;
				} else {
					extract($readSeo->getResult()[0]);
					$this->seoData = $readSeo->getResult()[0];
					$this->data = [$empresa_title . ' - ' . SITENAME, $empresa_sobre, HOME . "/empresa/{$empresa_name}", HOME . "/uploads/{$empresa_capa}"];

					//EMPRESA:: empresa_views
					$arrUpdate = ['empresa_views' => $empresa_views + 1];
					$update = new Update;
					$update->exeUpdate('app_empresas', $arrUpdate, 'WHERE empresa_id = :empresaid', "empresaid={$empresa_id}");
				}
				break;

			//SEO:: CADASTRA EMPRESAf	
			case 'cadastra-empresa':
				$this->data = ['Cadastre sua empresa - ' . SITENAME, 'Página modelo para cadastro de empresas no portal', HOME . '/cadastra-empresa', INCLUDE_PATH . '/images/site.png'];
				break;

			//SEO:: INDEX	
			case 'index':
				$this->data = [SITENAME . ' - Seu guia de empresas, eventos e baladas!', SITEDESC, HOME, INCLUDE_PATH . '/images/site.png'];
				break;

			default :
				$this->data = ['404 Oppsss, Nada encontrado!', SITEDESC, HOME . '/404', INCLUDE_PATH . '/images/404.png'];
		}//EndSwitch

		if ($this->data) {
			$this->setTags();
		}
	}

	private function setTags() {
		$this->tags['title'] = $this->data[0];
		$this->tags['content'] = check::limitaString(html_entity_decode($this->data[1]), 25);
		$this->tags['link'] = $this->data[2];
		$this->tags['image'] = $this->data[3];

		$this->tags = array_map('strip_tags', $this->tags);
		$this->tags = array_map('trim', $this->tags);

		$this->data = null;

		//NORMAL PAGE
		$this->seoTags = '<title>' . $this->tags['title'] . '</title>' . "\n";
		$this->seoTags .= '<meta name ="description" content="' . $this->tags['content'] . '"/>' . "\n";
		$this->seoTags .= '<meta name="robots" content "index, follow" />' . "\n";
		$this->seoTags .= '<link rel="canonical" href="' . $this->tags['link'] . '"/>' . "\n";
		$this->seoTags .= "\n";

		//FACEBOOK
		$this->seoTags .='<meta property="og:site_name" content="' . SITENAME . '"/>' . "\n";
		$this->seoTags .='<meta property="og:locale" content= "pt_BR" />' . "\n";
		$this->seoTags .='<meta property="og:title" content= "' . $this->tags['title'] . '"/>' . "\n";
		$this->seoTags .='<meta property="og:description" content= "' . $this->tags['content'] . '" />' . "\n";
		$this->seoTags .='<meta property="og:image" content= "' . $this->tags['image'] . '" />' . "\n";
		$this->seoTags .='<meta property="og:url" content= "' . $this->tags['link'] . '" />' . "\n";
		$this->seoTags .='<meta property="og:type" content= "article" />' . "\n";
		$this->seoTags .= "\n";

		//TWITTER
		$this->seoTags .= '<meta itemprop="name" content="' . $this->tags['title'] . '"/>';
		$this->seoTags .= '<meta itemprop="description" content="' . $this->tags['content'] . '"/>';
		$this->seoTags .= '<meta itemprop="url" content="' . $this->tags['link'] . '"/>';

		$this->tags = null;
	}

}
