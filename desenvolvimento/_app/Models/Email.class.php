<?php

require('_app/Library/PHPMailer/class.phpmailer.php');
require_once ('_app/Library/PHPMailer/class.smtp.php');

/**
 * Email [ MODEL ]
 * Responsável por configurar a PHPMailer, validar os dados e disparar e-mails do sistema.
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Email {

	/** @var PHPMailer	 */
	private $mail;

	/** EMAIL DATA */
	private $data;

	/** CORPO DO EMAIL */
	private $assunto;
	private $mensagem;

	/** REMETENTE */
	private $remetendeNome;
	private $remetendeEmail;

	/** DESTINO */
	private $destinoNome;
	private $destinoEmail;

	/** CONTROLE */
	private $error;
	private $result;

	function __construct() {
		$this->mail = new PHPMailer;
		$this->mail->Host = MAILHOST;
		$this->mail->Port = MAILPORT;
		$this->mail->Username = MAILUSER;
		$this->mail->Password = MAILPASS;
		$this->mail->CharSet = 'UTF-8';
	}

	public function enviar(array $data) {
		$this->data = $data;
		$this->clear();

		if (in_array('', $this->data)) {
			$this->error = ['Erro ao enviar mensagem: para enviar esse e-mail, por favor preencha os campos requisitados!', WS_ALERT];
			$this->result = false;
		} elseif (!Check::validaEmail($this->data['RemetenteEmail'])) {
			$this->error = ['Erro ao enviar mensagem: o e-mail que você informou não tem um formato válido. Informe seu e-mail!', WS_ALERT];
			$this->result = false;
		} else {
			$this->setEmail();
			$this->config();
			$this->sendMail();
		}
	}

	function getResult() {
		return $this->result;
	}

	function getError() {
		return $this->error;
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	private function clear() {
		array_map('strip_tags', $this->data);
		array_map('trim', $this->data);
	}

	private function setEmail() {
		$this->assunto = $this->data['Assunto'];
		$this->mensagem = $this->data['Mensagem'];
		$this->remetendeNome = $this->data['RemetenteNome'];
		$this->remetendeEmail = $this->data['RemetenteEmail'];
		$this->destinoNome = $this->data['DestinoNome'];
		$this->destinoEmail = $this->data['DestinoEmail'];

		$this->data = null;
		$this->setMensagem();
	}

	private function setMensagem() {
		$this->mensagem = "{$this->mensagem}<hr><small>Recebida em:" . date('d/m/Y H:i') . " </small>";
	}

	private function config() {
		//SMTP AUTH
		$this->mail->IsSMTP();
		$this->mail->SMTPAuth = true;
		$this->mail->SMTPSecure = 'tls';
		$this->mail->IsHTML();

		//REMETENTE E RETORNO
		$this->mail->From = MAILUSER;
		$this->mail->FromName = $this->remetendeNome;
		$this->mail->AddReplyTo($this->remetendeEmail, $this->remetendeNome);

		//ASSUNTO, MENSAGEM E DESTINO
		$this->mail->Subject = $this->assunto;
		$this->mail->Body = $this->mensagem;
		$this->mail->addAddress($this->destinoEmail, $this->destinoNome);
	}

	private function sendMail() {
		if ($this->mail->send()) {
			$this->error = ['Obrigado por entrar em contato: Recebemos sua mensagem, e estaremos respondendo em breve!', WS_ACCEPT];
			$this->result = true;
		} else {
			$this->error = ["Erro ao enviar: entre em contato com o admin.", WS_ERROR];
			$this->result = true;
		}
	}

}
