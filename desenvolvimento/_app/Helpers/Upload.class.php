<?php

/**
 * Upload.class [ HELPER ]
 * Classe responsável por executar upload de imagens, arquivos e midias no sistema.
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Upload {

	private $file;
	private $name;
	private $send;

	/** IMAGE UPLOAD */
	private $width;
	private $image;

	/** RESULTSET */
	private $result;
	private $error;

	/** DIRETÓRIOS */
	private $folder;
	private static $baseDir;

	function __construct($baseDir = null) {
		self:: $baseDir = ((string) $baseDir ? $baseDir : '../uploads/');
		if (!file_exists(self::$baseDir) && !is_dir(self::$baseDir)) {
			mkdir(self::$baseDir, 0777);
		}
	}

	/**
	 * <b>image: </b> Método responsável por realizar o upload da imagem para o servidor
	 * 
	 * @param array  $image = $_FILES da imagem upada no formulario.
	 * @param STRING $name = Nome da imagem
	 * @param STRING $width = Tamanho (a ser redimensionado)
	 * @param STRING $folder = Pasta destino
	 */
	public function image(array $image, $name = null, $folder = null, $width = null) {
		$this->file = $image;
		$this->name = ((string) $name ? $name : substr($image['name'], 0, strrpos($image['name'], '.')));
		$this->width = ((int) $width ? $width : 1024);
		$this->folder = ((string) $folder ? $folder : 'images');

		$this->checkFolder($this->folder);
		$this->setFileName();
		$this->uploadImage();
	}

	/**
	 * <b>file: </b> Método responsável por realizar o upload de arquivos do tipo PDF e DOCx para o servidor.
	 * 
	 * @param ARRAY $file = Dados do arquivo
	 * @param STRING $name = Nome do arquivo
	 * @param STRING $folder = Pasta destino
	 * @param INT $maxFileSize = Tamanho de upload maximo permitido por arquivo
	 */
	public function file(array $file, $name = null, $folder = null, $maxFileSize = null) {
		$this->file = $file;
		$this->name = ((string) $name ? $name : substr($file['name'], 0, strrpos($file['name'], '.')));
		$this->folder = ((string) $folder ? $folder : 'files');
		$maxFileSize = ((int) $maxFileSize ? $maxFileSize : 2);

		$fileAccept = [
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/pdf',
		];

		if ($this->file['size'] > ($maxFileSize * (1024 * 1024))) {
			$this->result = false;
			$this->error = "Arquivo muito grande, tamanho máximo permitido de {$maxFileSize}mb";
		} elseif (!in_array($this->file['type'], $fileAccept)) {
			$this->result = false;
			$this->error = 'Tipo de arquivo inválido, mas você pode enviar arquivos .DOCX ou .PDF!';
		} else {
			$this->checkFolder($this->folder);
			$this->setFileName();
			$this->moveFile();
		}
	}

	/**
	 * <b>media: </b> Método responsavel por realizar o upload de arquivos de midia dos tipos MP3e MP4 para o servidor.
	 * 
	 * @param ARRAY $media = Dados do arquivo
	 * @param STRING $name = Nome do arquivo
	 * @param STRING $folder = Pasta destino
	 * @param INT $maxFileSize = Tamanho de upload maximo permitido por arquivo
	 */
	public function media(array $media, $name = null, $folder = null, $maxFileSize = null) {
		$this->file = $media;
		$this->name = ((string) $name ? $name : substr($media['name'], 0, strrpos($media['name'], '.')));
		$this->folder = ((string) $folder ? $folder : 'medias');
		$maxFileSize = ((int) $maxFileSize ? $maxFileSize : 40);

		$fileAccept = [
			'audio/mp3',
			'video/mp4',
		];

		if ($this->file['size'] > ($maxFileSize * (1024 * 1024))) {
			$this->result = false;
			$this->error = "Arquivo muito grande, tamanho máximo permitido de {$maxFileSize}mb";
		} elseif (!in_array($this->file['type'], $fileAccept)) {
			$this->result = false;
			$this->error = 'Tipo de arquivo inválido, mas você pode enviar audio .MP3 ou vídeo .MP4!';
		} else {
			$this->checkFolder($this->folder);
			$this->setFileName();
			$this->moveFile();
		}
	}

	/**
	 * 
	 * @return STRING = Retorna o caminho do arquivo ou, FALSE se der erro.
	 */
	function getResult() {
		return $this->result;
	}

	/**
	 * 
	 * @return STRING = Retorna o erro disparado.
	 */
	function getError() {
		return $this->error;
	}

	########################################
	############ PRIVATE METHODS ###########
	########################################

	private function checkFolder($folder) {
		list($y, $m) = explode('/', date('Y/m'));
		$this->createFolder("{$folder}");
		$this->createFolder("{$folder}/{$y}");
		$this->createFolder("{$folder}/{$y}/{$m}/");
		$this->send = "{$folder}/{$y}/{$m}/";
	}

	private function createFolder($folder) {
		if (!file_exists(self::$baseDir . $folder) && !is_dir(self::$baseDir . $folder)) {
			mkdir(self::$baseDir . $folder, 0777);
		}
	}

	private function setFileName() {
		$fileName = check::validaNome($this->name) . strrchr($this->file['name'], '.');
		if (file_exists(self::$baseDir . $this->send . $fileName)) {
			$fileName = check::validaNome($this->name) . '-' . time() . strrchr($this->file['name'], '.');
		}
		$this->name = $fileName;
	}

	//realiza o upload de imagens, redimensionando a mesma
	private function uploadImage() {
		switch ($this->file['type']) {
			//JPEG
			case 'image/jpg':
			case 'image/jpeg':
			case 'image/pjpeg':
				$this->image = imagecreatefromjpeg($this->file['tmp_name']);
				break;
			//PNG
			case 'image/png':
			case 'image/x-png':
				$this->image = imagecreatefrompng($this->file['tmp_name']);
				break;
		}

		if (!$this->image) {
			$this->result = false;
			$this->error = 'Tipo de arquivo inválido, mas você pode enviar imagens .JPG ou .PNG!';
		} else {
			$x = imagesx($this->image);
			$y = imagesy($this->image);
			$imageX = ($this->width < $x ? $this->width : $x);
			$imageH = ($imageX * $y) / $x;

			$newImage = imagecreatetruecolor($imageX, $imageH);
			imagealphablending($newImage, false);
			imagesavealpha($newImage, true);
			imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $imageX, $imageH, $x, $y);

			switch ($this->file['type']) {
				//JPEG
				case 'image/jpg':
				case 'image/jpeg':
				case 'image/pjpeg':
					imagejpeg($newImage, self::$baseDir . $this->send . $this->name);
					break;
				//PNG
				case 'image/png':
				case 'image/x-png':
					imagepng($newImage, self::$baseDir . $this->send . $this->name);
					break;
			}

			if (!$newImage) {
				$this->result = false;
				$this->error = 'Tipo de arquivo inválido, mas você pode enviar imagens JPG ou PNG!';
			} else {
				$this->result = $this->send . $this->name;
				$this->error = null;
			}

			imagedestroy($this->image);
			imagedestroy($newImage);
		}
	}
	

	//Envia arquivos e midias
	private function moveFile() {
		if (move_uploaded_file($this->file['tmp_name'], self::$baseDir . $this->send . $this->name)) {
			$this->result = $this->send . $this->name;
			$this->error = null;
		} else {
			$this->result = FALSE;
			$this->error = 'Erro ao tentar mover o arquivo. Favor tente mais tarde!';
		}
	}

}
