<?php

/**
 * View.class [ HELPER MVC ]
 *  Responsável por carregar o template, povoar e exibir a view, povoar e incluir arquivos PHP no sistema.
 * Arquitetura MVC!
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class View {

	private $data;
	private $keys;
	private $values;
	private $template;

	/**
	 * <b>LOAD: </b> Realiza o carregamento do template da página;
	 * 
	 * @param STRING $template = Nome do arquivo html do template, sem a extensão!
	 */
	public function load($template) {
		$this->template = REQUIRE_PATH . DIRECTORY_SEPARATOR . '_tpl' . DIRECTORY_SEPARATOR . (string) $template;
		$this->template = file_get_contents($this->template . '.tpl.html');
		return $this->template;
	}

	/**
	 * <b>show: </b> Substitui as keys do template, pelos resultados obtidos no banco de dados!
	 * 
	 * @param ARRAY $data = Dados retornados pelo banco de dados com a função getResult()
	 */
	public function show($data, $view) {
		$this->setKeys($data);
		$this->setValues();
		$this->showView($view);
	}

	/**
	 * <b>request: </b> Extrai os dados do array e subistitui nas variaveis do template.inc.php
	 * 
	 * @param STRING $file = Nome do arquivo php.inc a ser incluido, sem a extensão!
	 * @param ARRRAY $data = Dados retornados pelo banco de dados com a função getResult()!
	 */
	public function request($file, array $data) {
		extract($data);
		require ("{$file}.inc.php");
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	private function setKeys($data) {
		$this->data = $data;
		$this->data['HOME'] = HOME;
		$this->keys = explode('&', '#' . implode("#&#", array_keys($this->data)) . '#');
		$this->keys[] = '#HOME#';
	}

	private function setValues() {
		$this->values = array_values($this->data);
	}

	private function showView($view) {
		$this->template = $view;
		echo str_replace($this->keys, $this->values, $this->template);
	}

}
