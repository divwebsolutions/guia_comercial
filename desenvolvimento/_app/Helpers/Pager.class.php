<?php

/**
 * Pager.class [ HELPER ]
 * Realiza a gestão e paginação de  resultados do sistema.
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Pager {

	/**  DEFINE O PAGER */
	private $page;
	private $limit;
	private $offset;

	/** REALIZA A LEITURA */
	private $tabela;
	private $termos;
	private $places;

	/** DEFINE O PAGINATOR */
	private $rows;
	private $link;
	private $maxLinks;
	private $firstPage;
	private $lastPage;

	/** RENDERIZA O PAGINATOR */
	private $paginator;

	/* CONTROL */
	private $url;

	function __construct($link, $firstPage = null, $lastPage = null, $maxLinks = null) {
		$this->link = (string) $link;
		$this->firstPage = ((string) $firstPage ? $firstPage : 'Primeira página');
		$this->lastPage = ((string) $lastPage ? $lastPage : 'Última página');
		$this->maxLinks = ((int) $maxLinks ? $maxLinks : 5 );
	}

	/**
	 * <b>exePager: </b> Calcula o offset para a página atual.
	 * 
	 * @param INT  $page  => Página atual;
	 * @param INT  $limit => Limite de resultados por página;
	 */
	public function exePager($page, $limit) {
		$this->page = ((int) $page ? $page : 1);
		$this->limit = (int) $limit;
		$this->offset = ($this->page * $limit) - $limit;
	}

	/**
	 * <b>returnPage: </b> Esta função impede que páginas sem resultados sejam exibidas, automaticamente
	 * retornando para a última página com resultados. 
	 */
	public function returnPage() {
		if ($this->page > 1) {
			$nPage = $this->page - 1;
			header("location: {$this->link}{$nPage}");
//			echo "<script>window.location.replace(\"{$this->link}1\");</script>";
		}
	}

	function getUrl() {
		return $this->url;
	}

	/**
	 * <b>getPage: </b> Retorna a página atual.
	 * 
	 * @return INT => Retorna a página atual.
	 */
	public function getPage() {
		return $this->page;
	}

	function getLink() {
		return $this->link;
	}

	/**
	 * <b>getLimit: </b> Retorna o limite de resultados por página.
	 * 
	 * @return INT => Retorna o limite.
	 */
	public function getLimit() {
		return $this->limit;
	}

	/**
	 * <b>getOffset: </b> Retorna o offset da query para cada página.
	 * 
	 * @return INT => Retorna o offset.;
	 */
	public function getOffset() {
		return $this->offset;
	}

	/**
	 * <b>exePaginator: </b> Armazena os parâmetros nos atributos da classe, e chama o método getSyntax().
	 * 
	 * @param STRING $tabela => Nome da tabela no banco.
	 * @param STRING $termos => Termos da consulta.
	 * @param STRING $parseString => Parâmetros dos termos da consulta.
	 */
	public function exePaginator($tabela, $termos = null, $parseString = null) {
		$this->tabela = (string) $tabela;
		$this->termos = (string) $termos;
		$this->places = (string) $parseString;
		$this->getSyntax();
	}

	public function getPaginator() {
		return $this->paginator;
	}

	########################################
	########### PRIVATE METHODS ############
	########################################
	//executa a consulta e retorna a estrutura do Fpaginator.

	private function getSyntax() {
		$read = new Read;
		$read->exeRead($this->tabela, $this->termos, $this->places);
		$this->rows = $read->getRowCount();

		if ($this->rows > $this->limit) {
			$paginas = ceil($this->rows / $this->limit);
			$maxLinks = $this->maxLinks;

			$this->paginator = "<ul class=\"paginator\">";
			$this->paginator .= "<li><a title=\"{$this->firstPage}\" href=\"{$this->link}1\">{$this->firstPage}</a></li>";

			for ($ipag = $this->page - $maxLinks; $ipag <= $this->page - 1; $ipag++) {
				if ($ipag >= 1) {
					$this->paginator .= "<li><a title=\"Página{$ipag}\" href=\"{$this->link}{$ipag}\">{$ipag}</a></li>";
				}
			}

			$this->paginator .= "<li><span class=\"activePage\">{$this->page}</span></li>";

			for ($dpag = $this->page + 1; $dpag <= $this->page + $maxLinks; $dpag++) {
				if ($dpag <= $paginas) {
					$this->paginator .= "<li><a title=\"Página{$dpag}\" href=\"{$this->link}{$dpag}\">{$dpag}</a></li>";
				}
			}

			$this->paginator .= "<li><a title=\"{$this->lastPage}\" href=\"{$this->link}{$paginas}\">{$this->lastPage}</a></li>";
			$this->paginator .= "</ul>";
		}
	}

}
