<?php

/**
 * Check.class [ HELPER ]
 * Classe responsável por manipular  e validar dados do sitema!
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Check {

	private static $data;
	private static $format;

	public static function validaEmail($email) {
		self::$data = (string) $email;
		self::$format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';
		//valida email usando expressão regular.
		if (preg_match(self::$format, self::$data)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <b>validaNome: </b> Formata e otimiza strings para serem armazenados no banco de dados.
	 * 
	 * @param STRING $nome => campo a ser formatado.
	 * @return STRING => retorna a string formatada e otimizada para o banco.
	 */
	public static function validaNome($nome) {
		self::$format = []; //array
		self::$format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
		self::$format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 '; //NÃO RETIRAR OS ESPAÇOS

		self::$data = strtr(utf8_decode($nome), utf8_decode(self::$format['a']), self::$format['b']);
		self::$data = strip_tags(trim(self::$data));
		self::$data = str_replace(' ', '-', self::$data);
		self::$data = str_replace(array('-----', '----', '---', '--'), '-', self::$data);

		return strtolower(utf8_encode(self::$data));
	}

	/**
	 * <b>data: </b> Formata datas de acordo com o padráo do mysql.
	 * @param  DATE $data => data a ser formatada
	 * @return DATE => retorna data formatada no padrão timestamp do mysql.
	 */
	public static function data($data) {
		self::$format = explode(' ', $data);
		self::$data = explode('/', self::$format[0]);

		if (empty(self::$format[1])) {
			self::$format[1] = date('H:i:s');
		}

		self::$data = self::$data[2] . '-' . self::$data[1] . '-' . self::$data[0] . ' ' . self::$format[1];
		return self::$data;
	}

	/**
	 * <b>limitaString: </b> Função que permite limitar as palavras de uma string a serem mostradas.
	 * 
	 * @param  STRING  $string => string a ser estilizada.
	 * @param  INT     $limite => limite de palavras da string
	 * @param  STRING  $pointer => Ponto de finalização da string.
	 * @return STRING => Retorna texto estilizado.
	 */
	public static function limitaString($string, $limite, $pointer = null) {
		self::$data = strip_tags(trim($string));
		self::$format = (int) $limite;

		$arrWords = explode(' ', self::$data);
		$numWords = count($arrWords);
		$newWords = implode(' ', array_slice($arrWords, 0, self::$format));

		$finalPointer = (empty($pointer) ? '...' : ' ' . $pointer);
		$result = (self::$format < $numWords ? $newWords . $finalPointer : self::$data);
		return $result;
	}

	/**
	 * <b>categoriaByName: </b> Função que permite consultar o ID de uma categoria, buscando-a pelo
	 * nome
	 * @param  STRING $nomeCategoria => Informe o nome da categoria a ser consultada.
	 * @return INT => Retorna o ID da categoria consultada.
	 */
	public static function categoriaByName($nomeCategoria) {
		$read = new Read;
		$read->exeRead('ws_categories', "WHERE category_name = :name", "name={$nomeCategoria }");

		if ($read->getRowCount()) {
			return $read->getResult()[0]['category_id'];
		} else {
			echo"A categoria <strong>{$nomeCategoria}</strong > não foi encontrada.";
			die;
		}
	}

	/**
	 * <b>userOnline: </b> Função que permite consultar quantos usuários estão ativos no sistema.
	 * 
	 * @return INT => Retorna o número de usuarios ativos.
	 */
	public static function userOnline() {
		$now = date('Y-m-d H:i:s');
		$deleteUserOnline = new Delete;
		$deleteUserOnline->exeDelete('ws_siteviews_online', "WHERE online_endview < :now", "now={$now}");

		$readUserOnline = new Read;
		$readUserOnline->exeRead('ws_siteviews_online');
		return $readUserOnline->getRowCount();
	}

	/**
	 * <b>image: </b> Função que permite redimensionar imagens.
	 *  
	 * @param STRING $imgUrl => url da imagem.
	 * @param STRING $imgDescricao => Breve descrição da imagem.
	 * @param STRING $imgWidht => Largura a ser adotada pela imagem.
	 * @param STRING $imgHeight =>altura a ser adotada pela imagem.
	 * @return HTML => Retorna codigo HTML de chamada da imagem redimensionada.
	 */
	public static function image($imgUrl, $imgDescricao, $imgWidht = null, $imgHeight = null) {
		self::$data = $imgUrl;

		if (file_exists(self::$data) && !is_dir(self::$data)) {
			$patch = HOME;
			$imagem = self::$data;
			return "<img src=\"{$patch}/tim.php?src={$patch}/{$imagem}&w={$imgWidht}&h={$imgHeight}\" alt=\"{$imgDescricao}\" title=\"{$imgDescricao}\" />";
		} else {
			return false;
		}
	}

	public static function validaUrl($url) {
		if (!filter_var($url, FILTER_VALIDATE_URL)) {
			return false;
		} else {
			return true;
		}
	}

}
