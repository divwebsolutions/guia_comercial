<?php

/**
 * Session.class [ HELPER ]
 * Responsável pelas estatísticas, sessões e atualizações de tráfego do sistema.
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class Session {

	private $date;
	private $cache;
	private $traffic;
	private $browser;

	function __construct($cache = null) {
		session_start(); //inicia a sessão
		$this->checkSession($cache);
	}

//Verifica e executa todos os métodos da classe!
	private function checkSession($cache = null) {
		$this->date = date('Y-m-d');
		$this->cache = ((int) $cache ? $cache : 20);

		if (empty($_SESSION['userOnline'])) {
			$this->setTraffic();
			$this->setSession();
			$this->checkBrowser();
			$this->setUsuario();
			$this->browserUpdate();
		} else {
			$this->trafficUpdate();
			$this->sessionUpdate();
			$this->checkBrowser();
			$this->usuarioUpdate();
		}

		$this->date = null;
	}

	/*
	 * ************************************** 
	 * *******   SESSÃO DO USUÁRIO   ********
	 * **************************************
	 */

//inicia a sessão do usuário
	private function setSession() {
		$_SESSION['userOnline'] = [
			"online_session" => session_id(),
			"online_startview" => date('Y-m-d H:i:s'),
			"online_endview" => date('Y-m-d H:i:s', strtotime("+{$this->cache}minutes")),
			"online_ip" => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP),
			"online_url" => filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_DEFAULT),
			"online_agent" => filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_DEFAULT),
		];
	}

//Atualiza sessão do usuário
	private function sessionUpdate() {
		$_SESSION['userOnline']['online_endview'] = date('Y-m-d H:i:s', strtotime("+{$this->cache}minutes"));
		$_SESSION['userOnline']['online_url'] = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_DEFAULT);
	}

	/*
	 * ***************************************
	 * *** USUÁRIOS, VISITAS, ATUALIZAÇÕES ***
	 * ***************************************
	 */

//Verifica e insere o trafego na tabela
	private function setTraffic() {
		$this->getTraffic();

		if (!$this->traffic) {
			$arrSiteViews = [
				'siteviews_date' => $this->date,
				'siteviews_users' => 1,
				'siteviews_views' => 1,
				'siteviews_pages' => 1,
			];

			$createSiteViews = new Create;
			$createSiteViews->exeCreate('ws_siteviews', $arrSiteViews);
		} else {

			if (!$this->getCookie()) {
				$arrSiteViews = [
					'siteviews_users' => $this->traffic['siteviews_users'] + 1,
					'siteviews_views' => $this->traffic['siteviews_views'] + 1,
					'siteviews_pages' => $this->traffic['siteviews_pages'] + 1,
				];
			} else {
				$arrSiteViews = [
					'siteviews_views' => $this->traffic['siteviews_views'] + 1,
					'siteviews_pages' => $this->traffic['siteviews_pages'] + 1,
				];
			}
			$updateSiteViews = new Update;
			$updateSiteViews->exeUpdate('ws_siteviews', $arrSiteViews, 'WHERE siteviews_date = :date', "date={$this->date}");
		}
	}

//Verifica e atualiza os pageviews
	private function trafficUpdate() {
		$this->getTraffic();
		$arrSiteViews = [
			'siteviews_pages' => $this->traffic['siteviews_pages'] + 1,
		];

		$updatePageViews = new Update;
		$updatePageViews->exeUpdate('ws_siteviews', $arrSiteViews, 'WHERE siteviews_date = :date', "date={$this->date}");
		$this->traffic = null;
	}

//Obtém dados da tabela [HELPER TRAFFIC]
//ws_siteviews
	private function getTraffic() {
		$readSiteViews = new Read;
		$readSiteViews->exeRead('ws_siteviews', 'WHERE siteviews_date = :date', "date={$this->date}");
		if ($readSiteViews->getRowCount()) {
			$this->traffic = $readSiteViews->getResult()[0];
		}
	}

//Verifica, cria e atualiza o cookie do usuário [HELPER TRAFFIC]
	private function getCookie() {
		$cookie = filter_input(INPUT_COOKIE, 'userOnline', FILTER_DEFAULT);
		setcookie("userOnline", base64_encode("DivWeb_Solutions"), time() + 86400);
		if (!$cookie) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * *************************************** 
	 * *******  NAVEGADORES DE ACESSO   ******
	 * ***************************************
	 */

//Identifica navegador do usuario
	private function checkBrowser() {
		$this->browser = $_SESSION['userOnline']['online_agent'];
		if (strpos($this->browser, 'Edge')) {
			$this->browser = 'Edge';
		} elseif (strpos($this->browser, 'MSIE') || strpos($this->browser, 'Trident/')) {
			$this->browser = 'IE';
		} elseif (strpos($this->browser, 'Firefox')) {
			$this->browser = 'Firefox';
		} elseif (strpos($this->browser, 'Chrome')) {
			$this->browser = 'Chrome';
		} else {
			$this->browser = 'Outros';
		}
	}

//Atualiza tabela com dados dos navegadores
	private function browserUpdate() {
		$readAgent = new Read;
		$readAgent->exeRead('ws_siteviews_agent', 'WHERE agent_name = :agent', "agent={$this->browser}");

		if (!$readAgent->getResult()) {
			$arrAgent = ['agent_name' => $this->browser, 'agent_views' => 1];
			$createAgent = new Create;
			$createAgent->exeCreate('ws_siteviews_agent', $arrAgent);
		} else {
			$arrAgent = ['agent_views' => $readAgent->getResult()[0]['agent_views'] + 1];
			$updateAgent = new Update;
			$updateAgent->exeUpdate('ws_siteviews_agent', $arrAgent, 'WHERE agent_name = :agent', "agent={$this->browser}");
		}
	}

	/*
	 * ************************************** 
	 * ********** USUÁRIOS ONLINE ***********
	 * **************************************
	 */

	//cadastra usuário online na tabela
	private function setUsuario() {
		$sesOnline = $_SESSION['userOnline'];
		$sesOnline['agent_name'] = $this->browser;
		$userCreate = new Create;
		$userCreate->exeCreate('ws_siteviews_online', $sesOnline);
	}

	//atualiza navegação do usuário online
	private function usuarioUpdate() {
		$arrOnline = [
			'online_endview' => $_SESSION['userOnline']['online_endview'],
			'online_url' => $_SESSION['userOnline']['online_url'],
		];

		$userUpdate = new Update;
		$userUpdate->exeUpdate('ws_siteviews_online', $arrOnline, 'WHERE online_session = :id', "id={$_SESSION['userOnline']['online_session']}");

		if (!$userUpdate->getRowCount()) {
			$readSesssion = new Read;
			$readSesssion->exeRead('ws_siteviews_online', 'WHERE online_session = :onid', "onid={$_SESSION['userOnline']['online_session']}");
			if (!$readSesssion->getRowCount()) {
				$this->setUsuario();
			}
		}
	}

}
