<?php
ob_start();
require ('./_app/config.inc.php');
$session = new Session;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">

        <!--[if lt IE 9]>
            <script src="../../_cdn/html5.js"></script>
         <![endif]--> 

		<?php
		$link = new Link;
		$link->getTags();
		?>

		<link rel="shortcut icon" href="themes/cidadeonline/images/ico_cidadeonline.png" type="image/x-icon" />
        <link rel="stylesheet" href="<?= INCLUDE_PATH; ?>/css/reset.css">
        <link rel="stylesheet" href="<?= INCLUDE_PATH; ?>/css/style.css">
        <link rel="stylesheet" href="<?= HOME; ?>/_cdn/shadowbox/shadowbox.css">
        <link href='http://fonts.googleapis.com/css?family=Baumans' rel='stylesheet' type='text/css'>

    </head>
    <body>
		<?php
		require(REQUIRE_PATH . '/inc/header.inc.php');

		if (!require ($link->getPatch())) {
			WSErro('Erro ao incluir arquivo de navegação, entre em contato com o admin!', WS_ERROR, true);
		}

		require(REQUIRE_PATH . '/inc/footer.inc.php');
		?>

    </body>

    <script src="<?= HOME ?>/_cdn/jquery.js"></script>
    <script src="<?= HOME ?>/_cdn/jcycle.js"></script>
    <script src="<?= HOME ?>/_cdn/jmask.js"></script>
    <script src="<?= HOME ?>/_cdn/shadowbox/shadowbox.js"></script>
    <script src="<?= HOME ?>/_cdn/_plugins.conf.js"></script>
    <script src="<?= HOME ?>/_cdn/_scripts.conf.js"></script>
	<script type="text/javascript" src="http://cidades-estados-js.googlecode.com/files/cidades-estados-v0.2.js"></script>

</html>
<?php
ob_end_flush();