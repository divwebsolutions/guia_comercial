<?php

/**
 * AdminCategory.class [ MODEL ADMIN ]
 * Responsavel por gerenciar as categorias do sistema no admin!
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class AdminCategory {

	private $data;
	private $catId;
	private $error;
	private $result;

	//Nome da tabela no banco de dados
	const ENTITY = 'ws_categories';

	/**
	 * <b>Cadastrar Nova Categoria: </b> método responsável por cadastrar novas categorias no sistema
	 *  
	 * @param ARRAY $data -> Informe um array associativo contendo os dados da categoria a ser criada. 
	 * (Proveniente de um formulário, atráves do INPUT_POST)
	 */
	public function exeCreate(array $data) {
		$this->data = $data;
		if (in_array('', $this->data)) {
			$tipo = (empty($this->data['category_parent']) ? 'seção' : 'categoria' );
			$this->error = ["<b>Erro ao cadastrar: </b> Para cadastrar uma nova {$tipo}, informe todos os campos!", WS_ALERT];
			$this->result = false;
		} else {
			$this->setData();
			$this->setName();
			$this->create();
		}
	}

	/**
	 * 
	 * @param INT $categoryId = ID(código) da categoria a ser atualizada
	 * @param ARRAY $data = Informe um array associativo contendo os novos dados da categoria a ser atualizada. 
	 * (Proveniente de um formulário, atráves do INPUT_POST)
	 */
	public function exeUpdate($categoryId, array $data) {
		$this->catId = (int) $categoryId;
		$this->data = $data;

		if (in_array('', $this->data)) {
			$tipo = (empty($this->data['category_parent']) ? 'seção' : 'categoria' );
			$this->error = ["<b>Erro ao atualizar: </b> Para atualizar a {$tipo} <b>{$this->data['category_title']}</b>, informe todos os campos!", WS_ALERT];
			$this->result = false;
		} else {
			$this->setData();
			$this->setName();
			$this->update();
		}
	}

	/**
	 * <b>Deletar categoria: </b> Método responsavel por deletar seções e categorias cadastradas no banco.
	 * 	basta informar o id da categoria.(Para excluir uma seção/categorias primeiro delete as dependências);
	 * 
	 * @param INT $categoryId = Informe o ID da categoria a ser deletada
	 */
	public function exeDelete($categoryId) {
		$this->catId = $categoryId;
		$readCat = new Read;
		$readCat->exeRead(self::ENTITY, 'WHERE category_id = :id', "id={$categoryId}");
		if (!$readCat->getResult()) {
			$this->result = false;
			$this->error = ['Opss...Você tentou excluir uma categoria que não existe!', WS_ALERT];
		} else {
			extract($readCat->getResult()[0]);
			if (!$category_parent && !$this->checkSubCats()) {
				//Não posso deletar
				$this->result = false;
				$this->error = ["A <b>Seção {$category_title}</b> possui vinculos com outras categorias, para continuar, exclua ou altere as categorias filhas!", WS_ALERT];
			} elseif ($category_parent && !$this->checkPosts()) {
				//Não pode deletar
				$this->result = false;
				$this->error = ["A <b>categoria {$category_title}</b> contém artigos vinculados, para continuar exclua ou altere todos os posts desta categoria!", WS_ALERT];
			} else {
				$this->delete();

				$tipo = (empty($category_parent) ? 'seção' : 'categoria');
				$this->result = true;
				$this->error = ["A <b>{$tipo} {$category_title}</b> foi deletada! ", WS_ACCEPT];
			}
		}
	}

	/**
	 * 
	 * @return BOOLEAN = retorna o ultimo ID cadastrado no banco, ou false caso retorne algum erro.
	 */
	function getResult() {
		return $this->result;
	}

	/**
	 * 
	 * @return ARRAY = Retorna a mensagem do erro e o tipo de erro.s
	 */
	function getError() {
		return $this->error;
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	//Verifica e garante que todos os campos estejam em suas formas corretas.
	private function setData() {
		$this->data = array_map('strip_tags', $this->data);
		$this->data = array_map('trim', $this->data);
		$this->data['category_name'] = Check::validaNome($this->data['category_title']);
		$this->data['category_date'] = Check::data($this->data['category_date']);
		$this->data['category_parent'] = ($this->data['category_parent'] == 'null' ? null : $this->data['category_parent'] );
	}

	//Verifica e garante que não irão existir duas categorias com o mesmo nome
	private function setName() {
		$where = (!empty($this->catId) ? "category_id != {$this->catId} AND" : '');

		$readName = new Read;
		$readName->exeRead(self::ENTITY, "WHERE {$where} category_title = :t", "t={$this->data['category_title']}");
		if ($readName->getResult()) {
			$this->data['category_name'] = $this->data['category_name'] . '-' . $readName->getRowCount();
		}
	}

	//Verifica as subcategorias da seção
	private function checkSubCats() {
		$readSes = new Read;
		$readSes->exeRead(self::ENTITY, 'WHERE category_parent = :parent', "parent={$this->catId}");
		if ($readSes->getResult()) {
			//existem subcategorias, não posso deletar
			return false;
		} else {
			//não existem subcategorias, posso deletar
			return true;
		}
	}

	//Verifica artigos da categoria
	private function checkPosts() {
		$readPost = new Read;
		$readPost->exeRead('ws_posts', 'WHERE post_category = :postCategory', "postCategory={$this->catId}");
		if ($readPost->getResult()) {
			//não pode deletar
			return false;
		} else {
			//pode deletar
			return true;
		}
	}

	//executa a inserção da categoria no banco de dados
	private function create() {
		$create = new Create;
		$create->exeCreate(self::ENTITY, $this->data);
		if ($create->getResult()) {
			$this->result = $create->getResult();
			$this->error = ["<b>Sucesso: </b> A categoria {$this->data['category_name']} foi cadastrada no sistema!", WS_ACCEPT];
		}
	}

	//Executa a atualização dos campos da categoria no banco de dados.
	private function update() {
		$update = new Update;
		$update->exeUpdate(self::ENTITY, $this->data, 'WHERE category_id = :id', "id={$this->catId}");
		if ($update->getResult()) {
			$tipo = (empty($this->data['category_parent']) ? 'seção' : 'categoria' );
			$this->result = true;
			$this->error = ["<b>Sucesso: </b> A {$tipo} {$this->data['category_name']} foi atualizada no sistema!", WS_ACCEPT];
		}
	}

	//Executa a exclusão de uma categoria ou seção do banco de dados
	private function delete() {
		$delete = new Delete;
		$delete->exeDelete(self::ENTITY, 'WHERE category_id = :id', "id={$this->catId}");
	}

}
