<?php

/**
 * AdminPost.class [ MODEL ADMIN ]
 * Responsável por gerenciar os posts do Admin no sistema
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class AdminPost {

	private $data;
	private $post;
	private $error;
	private $result;

	//Nome da tabela no banco de dados
	const ENTITY = 'ws_posts';

	/**
	 * <b>Cadastra Post: </b> Executa a inserção de um post no sistema
	 * 
	 * @param ARRAY $data = Informe um array associativo com os dados do post a ser cadastrado (Proveniente do formulário preenchido pelo usuário). 	 
	 */
	public function exeCreate(array $data) {
		$this->data = $data;
		if (in_array('', $this->data)) {
			$this->error = ["Para criar um novo post, favor preencha todos os campos!", WS_ALERT];
			$this->result = false;
		} else {
			$this->setData();
			$this->setName();

			if ($this->data['post_cover']) {
				$upload = new Upload;
				$upload->image($this->data['post_cover'], $this->data['post_name']);
			}

			if (isset($upload) && $upload->getResult()) {
				$this->data['post_cover'] = $upload->getResult();
				$this->createPost();
			} else {
				$this->data['post_cover'] = null;
				$this->createPost();
			}
		}
	}

	public function exeUpdate($postId, array $data) {
		$this->post = (int) $postId;
		$this->data = $data;

		if (in_array('', $this->data)) {
			$this->error = ["<b>Erro ao atualizar: </b> Para atualizar o post <b>{$this->data['post_title']}</b>, informe todos os campos! (Campo capa não é obrigatório)", WS_ALERT];
			$this->result = false;
		} else {
			$this->setData();
			$this->setName();
			//Se eu adicionar uma nova capa, eu procuro a antiga e deleto da pasta
			if (is_array($this->data['post_cover'])) {
				$read = new Read;
				$read->exeRead(self::ENTITY, 'WHERE post_id = :id', "id={$this->post}");
				$capa = '../uploads/' . $read->getResult()[0]['post_cover'];

				if (file_exists($capa) && !is_dir($capa)) {
					unlink($capa);
				}

				//Agora, envio a nova capa para o servidor
				$uploadNovaCapa = new Upload;
				$uploadNovaCapa->image($this->data['post_cover'], $this->data['post_name']);
			}
			
			if (isset($uploadNovaCapa) && $uploadNovaCapa->getResult()) {
				$this->data['post_cover'] = $uploadNovaCapa->getResult();
				$this->Update();
			} else {
				unset($this->data['post_cover']);
				$this->Update();
			}
		}
	}

	public function exeDelete($postId) {
		$this->post = (int) $postId;

		//Remover Imagens
		$readPost = new Read;
		$readPost->exeRead(self::ENTITY, 'WHERE post_id = :postid', "postid={$this->post}");
		if (!$readPost->getResult()) {
			$this->error = ["O post que você tentou excluir não existe no sistema!", WS_ERROR];
			$this->result = false;
		} else {
			$postDelete = $readPost->getResult()[0];
			if (file_exists('../uploads/' . $postDelete['post_cover']) && !is_dir('../uploads/' . $postDelete['post_cover'])) {
				unlink('../uploads/' . $postDelete['post_cover']);
			}

			//exclui gallery do post
			$readGallery = new Read;
			$readGallery->exeRead('ws_posts_gallery', 'WHERE post_id = :postid', "postid={$this->post}");
			if ($readGallery->getResult()) {
				foreach ($readGallery->getResult() as $gbDel) {
					if (file_exists('../uploads/' . $gbDel['gallery_image']) && !is_dir('../uploads/' . $gbDel['gallery_image'])) {
						unlink('../uploads/' . $gbDel['gallery_image']);
					}
				}
			}

			//Deleta do banco de dados
			$delete = new Delete;
			$delete->exeDelete('ws_posts_gallery', 'WHERE post_id = :postid', "postid={$this->post}");
			$delete->exeDelete(self::ENTITY, 'WHERE post_id = :postid', "postid={$this->post}");

			$this->error = ["O post <b>{$postDelete['post_title']}</b> foi removido com sucesso do sistema!", WS_ACCEPT];
			$this->result = true;
		}
	}

	/**
	 * <b>Atualiza Status do post: </b> Atualiza o post para rascunho ou post ativo.
	 * 
	 * @param type $postId = id do post a ser atualizado.
	 * @param type $postStatus = 1= Ativo / 0= Inativo(rascunho).
	 */
	public function exeStatus($postId, $postStatus) {
		$this->post = (int) $postId;
		$this->data['post_status'] = (string) $postStatus;
		$update = new Update;
		$update->exeUpdate(self::ENTITY, $this->data, 'WHERE post_id = :postid', "postid={$this->post}");
	}

	/**
	 * <b>Envia Galeria: </b>	Envelope um $_FILES de um input file multiples e envie junto a um postId para executar 
	 * o upload e o cadastro  de galerias do artigo.
	 * 
	 * @param ARRAY $images = Envie um $_FILES multiple
	 * @param INT $postId = Informe o ID do post
	 */
	public function gbSend(array $images, $postId) {
		$this->post = (int) $postId;
		$this->data = $images;

		$galleryName = new Read;
		$galleryName->exeRead(self::ENTITY, 'WHERE post_id = :id', "id={$this->post}");
		if (!$galleryName->getResult()) {
			$this->error = ["Erro ao enviar galeria: O índice {$this->post} não foi encontrado no banco de dados!", WS_ERROR];
			$this->result = false;
		} else {
			$galleryName = $galleryName->getResult()[0]['post_name'];

			$gbFiles = [];
			$gbCount = count($this->data['tmp_name']);
			$gbKeys = array_keys($this->data);

			for ($gb = 0; $gb < $gbCount; $gb++) {
				foreach ($gbKeys as $keys) {
					$gbFiles[$gb]{$keys} = $this->data[$keys][$gb];
				}
			}

			$gbSend = new Upload;
			$i = 0;
			$u = 0;

			foreach ($gbFiles as $gbUpload) {
				$i++;
				$imgName = "{$galleryName}-gb-{$this->post}-" . substr(md5(time() + $i), 0, 5);
				$gbSend->image($gbUpload, $imgName);

				if ($gbSend->getResult()) {
					$gbImage = $gbSend->getResult();
					$gbCreate = [
						'post_id' => $this->post,
						'gallery_image' => $gbImage,
						'gallery_date' => date('Y-m-d H:i:s'),
					];
					$insertGb = new Create;
					$insertGb->exeCreate('ws_posts_gallery', $gbCreate);
					$u++;
				}
			}

			if ($u > 1) {
				$this->error = ["Galeria Atualizada: Foram enviadas {$u} imagens para a galeria deste post", WS_ACCEPT];
				$this->result = true;
			}
		}
	}

	public function gbRemove($gbImageId) {
		$this->post = (int) $gbImageId;
		$readGb = new Read;
		$readGb->exeRead('ws_posts_gallery', 'WHERE gallery_id = :gbId', "gbId={$this->post}");
		if ($readGb->getResult()) {
			$imagem = '../uploads/' . $readGb->getResult()[0]['gallery_image'];
			if (file_exists($imagem) && !is_dir($imagem)) {
				unlink($imagem);
			}
			$deleteImage = new Delete;
			$deleteImage->exeDelete('ws_posts_gallery', 'WHERE gallery_id = :gbId', "gbId={$this->post}");
			if ($deleteImage->getResult()) {
				$this->error = ["Sucesso: A imagem foi excluida!", WS_ACCEPT];
				$this->result = true;
			}
		}
	}

	/**
	 * 
	 * @return BOOLEAN/INT = Se der erro, retorna false, senão, retorna o id do ultimo post cadastrado no sistema
	 */
	function getResult() {
		return $this->result;
	}

	/**
	 * 
	 * @return ARRAY = retorna a mensagem no indice [0] e o tipo no indice [1]
	 */
	function getError() {
		return $this->error;
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	//Verifica, prepara e armazena os dados nos campos do array
	private function setData() {
		//Separei para poder aplicar o strip_tags e o trim sem afetar o cover e o content
		$cover = $this->data['post_cover'];
		$content = $this->data['post_content'];
		unset($this->data['post_cover'], $this->data['post_content']);

		$this->data = array_map('strip_tags', $this->data);
		$this->data = array_map('trim', $this->data);

		$this->data['post_name'] = Check::validaNome($this->data['post_title']);
		$this->data['post_date'] = Check::data($this->data['post_date']);
		$this->data['post_type'] = 'post';

		//Retornei os dados para o array
		$this->data['post_cover'] = $cover;
		$this->data['post_content'] = $content;

		$this->data['post_cat_parent'] = $this->getCatParent();
	}

	//Retorna a seção que está vinculada ao post
	private function getCatParent() {
		$rCat = new Read;
		$rCat->exeRead('ws_categories', 'WHERE category_id = :id', "id={$this->data['post_category']}");
		if ($rCat->getResult()) {
			return $rCat->getResult()[0]['category_parent'];
		} else {
			return null;
		}
	}

	//Verifica e garante que não irão existir dois posts com o mesmo nome
	private function setName() {
		$where = (isset($this->post) ? "post_id != {$this->post} AND" : '' );

		$readName = new Read;
		$readName->exeRead(self::ENTITY, "WHERE {$where} post_title = :t", "t={$this->data['post_title']}");
		if ($readName->getResult()) {
			$this->data['post_name'] = $this->data['post_name'] . '-' . $readName->getRowCount();
		}
	}

	//Executa a criação do post
	private function createPost() {
		$cadastraPost = new Create();
		$cadastraPost->exeCreate(self::ENTITY, $this->data);
		if ($cadastraPost->getResult()) {
			//deu certo
			$this->error = ["O post <b>{$this->data['post_title']}</b> foi cadastrado com sucesso no sistema!", WS_ACCEPT];
			$this->result = $cadastraPost->getResult();
		}
	}

	//Executa update do post
	private function Update() {
		$updatePost = new Update;
		$updatePost->exeUpdate(self::ENTITY, $this->data, 'WHERE post_id = :postId', "postId={$this->post}");
		if ($updatePost->getResult()) {
			//deu certo
			$this->error = ["O post <b>{$this->data['post_title']}</b> foi atualizado com sucesso no sistema!", WS_ACCEPT];
			$this->result = true;
		}
	}

}
