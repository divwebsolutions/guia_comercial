<?php

/**
 * AdminUsers.class [ MODEL ADMIN ]
 * Classe responsável pela manipulação dos usuários do sistema.
 * 
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class AdminUsers {

	private $data;
	private $userId;
	private $error;
	private $result;

	//Nome da tabela no banco de dados
	const ENTITY = 'ws_users';

	/**
	 * <b>Cadastrar Usuário: </b> método responsável por cadastrar novos usuarios no sistema
	 *  
	 * @param ARRAY $data -> Informe um array associativo contendo os dados do usuário a ser criado. 
	 * (Proveniente de um formulário, atráves do INPUT_POST);
	 */
	public function exeCreate(array $data) {
		$this->data = $data;

		if (in_array('', $this->data)) {
			$this->error = ["<b>Erro ao cadastrar: </b> Para cadastrar um novo usuário, informe todos os campos!", WS_ALERT];
			$this->result = false;
		} else {
			if (!$this->validaEmail($this->data['user_email'])) {
				$this->error = ["<b>Erro ao cadastrar: </b> E-mail inválido, ou já cadastrado!", WS_ALERT];
				$this->result = null;
			} else {
				$this->setData();
				$this->create();
			}
		}
	}

	public function exeUpdate($userId, array $data) {
		$this->userId = (int) $userId;
		$this->data = $data;

		if (in_array('', $this->data)) {
			$this->error = ["<b>Erro ao cadastrar: </b> Para cadastrar um novo usuário, informe todos os campos!", WS_ALERT];
			$this->result = false;
		} else {
			$this->setData();
			$this->update();
		}
	}

	public function exeDelete($userId) {
		$this->userId = $userId;
		$readUser = new Read;
		$readUser->exeRead(self::ENTITY, 'WHERE user_id = :id', "id={$this->userId}");
		if (!$readUser->getResult()) {
			$this->error = ["Erro: você tentou deletar um usuário que não existe!", WS_ALERT];
		} else {
			if ($readUser->getResult()[0]['user_level'] == 3) {
				//verificar se é o ultimo admin
				$readAdmin = new Read;
				$readAdmin->exeRead(self::ENTITY, 'WHERE user_level = :level', "level=3");

				if ($readAdmin->getRowCount() <= 1) {
					$this->error = ["Você não pode excluir todos os <b>Admins</b>", WS_ERROR];
					$this->result = false;
				} else {
					$this->delete();
				}
			} else {
				$this->delete();
			}
		}
	}

	function getResult() {
		return $this->result;
	}

	function getError() {
		return $this->error;
	}

	/**
	 * ****************************************
	 * ************ PRIVATE METHODS ***********
	 * ****************************************
	 */
	private function validaEmail($email) {
		if (Check::validaEmail($email)) {
			//email válido
			//Verifico no banco se já está cadastrado
			$readEmail = new Read;
			$readEmail->exeRead(self::ENTITY, ' WHERE user_email = :email', "email={$email}");
			if ($readEmail->getResult()) {
				//e-mail já cadastrado
				return false;
			} else {
				return true;
			}
		} else {
			//email inválido
			return false;
		}
	}

	private function setData() {
		$this->setSenha();
		array_map('strip_tags', $this->data);
		array_map('trim', $this->data);
	}

	private function setSenha() {
		if ($this->data['user_password'] == 'vazio') {
			unset($this->data['user_password']);
		} else {
			$this->data['user_password'] = md5($this->data['user_password']);
		}
	}

	//executa a inserção do usuário no banco de dados
	private function create() {
		$create = new Create;
		$create->exeCreate(self::ENTITY, $this->data);
		if ($create->getResult()) {
			$this->result = $create->getResult();
			$this->error = ["<b>Sucesso: </b> o usuário {$this->data['user_name']} foi cadastrado no sistema!", WS_ACCEPT];
		}
	}

	//Executa a atualização dos campos do usuário no banco de dados.
	private function update() {
		$update = new Update;
		$update->exeUpdate(self::ENTITY, $this->data, 'WHERE user_id = :id', "id={$this->userId}");
		if ($update->getResult()) {
			$this->error = ["<b>Sucesso: </b> O usuário <b>{$this->data['user_name']}</b> foi atualizado no sistema!", WS_ACCEPT];
			$this->result = true;
		}
	}

	//Executa a exclusão do usuário no sistema
	private function delete() {
		$deleta = new Delete;
		$deleta->exeDelete(self::ENTITY, 'WHERE user_id = :id', "id={$this->userId}");
		$this->error = ["Sucesso: o usuário foi deletado do sistema!", WS_ACCEPT];
		$this->result = true;
	}

}
