<?php

/**
 * AdminEmpresa.class [ TIPO ]
 * Descricao
 * @copyright (c) 2016, David A. Simoes DIVWEB SOLUTIONS
 */
class AdminEmpresa {

	private $data;
	private $empresa;
	private $error;
	private $result;

	//Nome da tabela no banco de dados
	const ENTITY = 'app_empresas';

	public function exeCreate(array $data) {
		$this->data = $data;
		//verifico se há campos vazios no array
		if (in_array('', $this->data)) {
			$this->error = ["Erro: para cadastrar uma nova empresa, preencha todos os campos!", WS_ALERT];
			$this->result = false;
		} else {
			$this->setData();

			if ($this->data['empresa_capa']) {
				$upload = new Upload;
				$upload->image($this->data['empresa_capa'], $this->data['empresa_name'], 'empresas');
			}

			if (isset($upload) && $upload->getResult()) {
				$this->data['empresa_capa'] = $upload->getResult();
				$this->create();
			} else {
				$this->data['empresa_capa'] = null;
				$this->create();
			}
		}
	}

	/**
	 * /**
	 * <b>Atualiza Empresa: </b> atualiza dados da  empresa no sistema!
	 * 
	 * @param INT $empresaId = ID da empresa a ser atualizada
	 * @param array $data = dados da empresa
	 */
	public function exeUpdate($empresaId, array $data) {
		$this->empresa = (int) $empresaId;
		$this->data = $data;

		//verifica se tem algum dado em branco	
		if (in_array('', $this->data)) {
			$this->error = ["<b>Erro:</b> para cadastrar uma nova empresa, preencha todos os campos<b>(capa não é obrigatório)</b>!", WS_ALERT];
			$this->result = false;
		} else {
			$this->setData();
			//Se eu adicionar uma nova capa, eu procuro a antiga e deleto da pasta
			if (is_array($this->data['empresa_capa'])) {
				$read = new Read;
				$read->exeRead(self::ENTITY, 'WHERE empresa_id = :id', "id={$this->empresa}");
				$capa = '../uploads/' . $read->getResult()[0]['empresa_capa'];

				if (file_exists($capa) && !is_dir($capa)) {
					unlink($capa);
				}

				//Agora, envio a nova capa para o servidor
				$uploadNovaCapa = new Upload;
				$uploadNovaCapa->image($this->data['empresa_capa'], $this->data['empresa_name'], 'empresas');
			}

			if (isset($uploadNovaCapa) && $uploadNovaCapa->getResult()) {
				$this->data['empresa_capa'] = $uploadNovaCapa->getResult();
				$this->Update();
			} else {
				unset($this->data['empresa_capa']);
				$this->Update();
			}
		}
	}

	/**
	 * <b>Deleta Empresa: </b> deleta empresa do sistema!
	 * 
	 * @param INT $empresaId = ID da empresa a ser deletada
	 */
	public function exeDelete($empresaId) {
		$this->empresa = (int) $empresaId;

		//Remover Imagens
		$readEmpresa = new Read;
		$readEmpresa->exeRead(self::ENTITY, 'WHERE empresa_id = :empresaid', "empresaid={$this->empresa}");
		if (!$readEmpresa->getResult()) {
			$this->error = ["A empresa que você tentou excluir não existe no sistema!", WS_ERROR];
			$this->result = false;
		} else {
			$empresaDelete = $readEmpresa->getResult()[0];
			if (file_exists('../uploads/' . $empresaDelete['empresa_capa']) && !is_dir('../uploads/' . $empresaDelete['empresa_capa'])) {
				unlink('../uploads/' . $empresaDelete['empresa_capa']);
			}

			//Deleta do banco de dados
			$delete = new Delete;
			$delete->exeDelete(self::ENTITY, 'WHERE empresa_id = :empresaid', "empresaid={$this->empresa}");

			$this->error = ["A empresa <b>{$empresaDelete['empresa_title']}</b> foi removida com sucesso do sistema!", WS_ACCEPT];
			$this->result = true;
		}
	}

	/**
	 * <b>Atualiza Status da empresa: </b> Atualiza a empresa para rascunho ou empresa ativa.
	 * 
	 * @param type $empresaId => id da empresa a ser atualizada.
	 * @param type $empresaStatus => 1= Ativo / 0= Inativo(rascunho).
	 */
	public function exeStatus($empresaId, $empresaStatus) {
		$this->empresa = (int) $empresaId;
		$this->data['empresa_status'] = (string) $empresaStatus;
		$update = new Update;
		$update->exeUpdate(self::ENTITY, $this->data, 'WHERE empresa_id = :empresaid', "empresaid={$this->empresa}");
	}

	/**
	 * 
	 * @return BOOLEAN/INT = Se der erro, retorna false, senão, retorna o id do ultimo post cadastrado no sistema
	 */
	function getResult() {
		return $this->result;
	}

	/**
	 * 
	 * @return ARRAY = retorna a mensagem no indice [0] e o tipo no indice [1]
	 */
	function getError() {
		return $this->error;
	}

	/**
	 * ****************************************
	 * *********** PRIVATE METHODS ************
	 * ****************************************
	 */
	private function setData() {
		$cover = $this->data['empresa_capa'];
		unset($this->data['empresa_capa']);

		$this->data = array_map('strip_tags', $this->data);
		$this->data = array_map('trim', $this->data);

		$this->data['empresa_name'] = Check::validaNome($this->data['empresa_title']);
		$this->data['empresa_date'] = Check::data(date('d/m/Y H:i:s'));

		if (!Check::validaUrl($this->data['empresa_site'])) {
			$this->data['empresa_site'] = null;
			$this->error = ["A url do site é inválida!", WS_ALERT];
			$this->result = null;
		}

		if (!Check::validaUrl($this->data['empresa_facebook'])) {
			$this->data['empresa_facebook'] = null;
			$this->error = ["A url da página é inválida!", WS_ALERT];
			$this->result = null;
		}

		$this->data['empresa_capa'] = $cover;
	}

	private function create() {
		$createEmpresa = new Create;
		$createEmpresa->exeCreate(self::ENTITY, $this->data);
		if ($createEmpresa->getResult()) {
			$this->error = ["Sucesso: a empresa {$this->data['empresa_title']} foi cadastrada no sistema! ", WS_ACCEPT];
			$this->result = $createEmpresa->getResult();
		}
	}

	//Executa update do post
	private function Update() {
		$updatePost = new Update;
		$updatePost->exeUpdate(self::ENTITY, $this->data, 'WHERE empresa_id = :empresaId', "empresaId={$this->empresa}");
		if ($updatePost->getResult()) {
			//deu certo
			$this->error = ["A empresa <b>{$this->data['empresa_title']}</b> foi atualizada com sucesso no sistema!", WS_ACCEPT];
			$this->result = true;
		}
	}

}
