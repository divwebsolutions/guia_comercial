	
<div class = "content form_create">

	<article>

		<header>
			<h1>Cadastrar Empresa:</h1>
		</header>

		<?php
		//recupero os dados do  formulário, via requisição
		$dataEmpresa = filter_input_array(INPUT_POST, FILTER_DEFAULT);
		if (isset($dataEmpresa) && $dataEmpresa['SendPostForm']) {
			$dataEmpresa['empresa_status'] = ($dataEmpresa['SendPostForm'] == 'Cadastrar' ? '0' : '1');
			$dataEmpresa['empresa_capa'] = ($_FILES['empresa_capa']['tmp_name'] ? $_FILES['empresa_capa'] : null);

			unset($dataEmpresa['SendPostForm']);

			require ('_models/AdminEmpresa.class.php');
			$cadastraEmpresa = new AdminEmpresa();
			$cadastraEmpresa->exeCreate($dataEmpresa);

			if (!$cadastraEmpresa->getResult()) {
				WSErro($cadastraEmpresa->getError()[0], $cadastraEmpresa->getError()[1]);
			} else {
				header('Location: painel.php?exe=empresas/update&create=true&empresaid=' . $cadastraEmpresa->getResult());
			}
		}
		?>

		<form name="PostForm" action="" method="post" enctype="multipart/form-data">

			<label class="label">
				<span class="field">Logo da empresa: <sup>Exatamente 578x288px (JPG ou PNG)</sup></span>
				<input type="file" name="empresa_capa" />
			</label>

			<label class="label">
				<span class="field">Nome da Empresa:</span>
				<input type="text" name="empresa_title" value="<?php if (isset($dataEmpresa['empresa_title'])) echo $dataEmpresa['empresa_title']; ?>"/>
			</label>

			<label class="label">
				<span class="field">Ramo de atividade:</span>
				<input type="text" name="empresa_ramo" value="<?php if (isset($dataEmpresa['empresa_ramo'])) echo $dataEmpresa['empresa_ramo']; ?>"/>
			</label>

			<label class="label">
				<span class="field">Sobre a empresa:</span>
				<textarea name="empresa_sobre" rows="3" ><?php if (isset($dataEmpresa['empresa_sobre'])) echo $dataEmpresa['empresa_sobre']; ?></textarea>
			</label>

			<label class="label">
				<span class="field">Nome da rua / Número:</span>
				<input type="text" name="empresa_endereco" placeholder="Rua 1 5677" value="<?php if (isset($dataEmpresa['empresa_endereco'])) echo $dataEmpresa['empresa_endereco']; ?>" />
			</label>

			<label class="label" style=" display: inline; float: left;">
				<span class="field">Site da empresa:</span>
				<input type="text" name="empresa_site"  placeholder="http://www.nome-da-empresa.com.br"  style="width: 450px;" value="<?php if (isset($dataEmpresa['empresa_site'])) echo $dataEmpresa['empresa_site']; ?>"/>
			</label>


			<label class="label" style=" display: inline; float: right;">
				<span class="field">FanPage/facebook da empresa:</span>
				<input type="text" name="empresa_facebook"  placeholder="https://www.facebook.com/nomedapagina" style="width: 400px;" value="<?php if (isset($dataEmpresa['empresa_facebook'])) echo $dataEmpresa['empresa_facebook']; ?>"/>
			</label>

			<div class="clear"></div>

			<div class="label_line">

				<label class="label_small">
					<span class="field">Estado UF:</span>
					<select class="j_loadstate" name="empresa_uf">
						<option value="" disabled selected> Selecione o estado </option>
						<?php
						$readState = new Read;
						$readState->ExeRead("app_estados", "ORDER BY estado_nome ASC");
						foreach ($readState->getResult() as $estado):
							extract($estado);
							echo "<option value=\"{$estado_id}\"> {$estado_uf} / {$estado_nome} </option>";
						endforeach;
						?>                        
					</select>
				</label>

				<label class="label_small">
					<span class="field">Cidade:</span>
					<select class="j_loadcity" name="empresa_cidade" disabled>
						<option value="" disabled selected> Selecione antes um estado </option>
					</select>
				</label>


				<label class="label_small">
					<span class="field">Indicação:</span> 
					<select name="empresa_categoria">
						<option value="" disabled selected> Indique a empresa como </option>
						<option value="onde-comer"> ONDE COMER </option>
						<option value="onde-ficar"> ONDE FICAR </option>
						<option value="onde-comprar"> ONDE COMPRAR </option>
						<option value="onde-se-divertir"> ONDE SE DIVERTIR </option>
					</select>
				</label>

			</div><!--/line-->


			<div class="gbform"></div>

			<input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />
			<input type="submit" class="btn green" value="Cadastrar & Publicar" name="SendPostForm" />

		</form>

	</article>

	<div class="clear"></div>
</div> <!-- content form- -->