<div class="content list_content">

    <section class="list_emp">

        <h1>Empresas:</h1>      

		<?php
		//validações
		$empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
		if ($empty) {
			WSErro("<b>Opsss...</b>Parece que você tentou editar uma empresa inexistente!", WS_ALERT);
		}

		//actions
		$action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);
		if ($action) {
			require ('_models/AdminEmpresa.class.php');
			$empresaAction = filter_input(INPUT_GET, 'empresa', FILTER_VALIDATE_INT);
			$empresaUpdate = new AdminEmpresa;

			//status 1=Ativo/0=Rascunho(inativo)
			switch ($action) {
				case 'active':
					$empresaUpdate->exeStatus($empresaAction, '1');
					WSErro("O status da empresa foi atualizado para <b>ativo</b>. Empresa publicado!", WS_ACCEPT);
					break;
				case 'inative':
					$empresaUpdate->exeStatus($empresaAction, '0');
					WSErro("O status da empresa foi atualizado para <b>inativo</b>. Empresa agora é um rascunho!", WS_ALERT);
					break;
				case 'delete':
					$empresaUpdate->exeDelete($empresaAction);
					WSErro($empresaUpdate->getError()[0], $empresaUpdate->getError()[1]);
					break;

				default :
					WSErro("Ação não foi identificada pelo sistema, favor utilize os botões!", WS_ERROR);
					break;
			}
		}

		$empresai = 0;

		//obtenho a pagina atual e instancio o paginator
		$getPage = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
		$pager = new Pager('painel.php?exe=empresas/index&page=');
		$pager->exePager($getPage, 6); //MODIFICAR AQUI O LIMITE DE EMPRESAS POR PÁGINA
		//Obtenho as empresas cadastradas, e ordeno para a paginação
		$readEmpresas = new Read;
		$readEmpresas->exeRead('app_empresas', 'WHERE empresa_status = 2 ORDER BY empresa_status ASC, empresa_date DESC LIMIT :limit OFFSET :offset', "limit={$pager->getLimit()}&offset={$pager->getOffset()}");

		//verifico se existem empresas cadastradas, e caso exista, eu mostro, uma a uma com foreach
		if ($readEmpresas->getResult()) {
			foreach ($readEmpresas->getResult() as $empresa) {
				extract($empresa);
				$status = (!$empresa_status ? 'style="background: #faf7b7"' : '');
				//leio nome da cidade
				$readEmpresas->exeRead('app_cidades', 'WHERE cidade_id = :cidadeId', "cidadeId={$empresa_cidade}");
				$empresa_cidade = $readEmpresas->getResult()[0]['cidade_nome'];
				//leio nome do estado
				$readEmpresas->exeRead('app_estados', 'WHERE estado_id = :estadoId', "estadoId={$empresa_uf}");
				$empresa_uf = $readEmpresas->getResult()[0]['estado_uf'];

				$empresai++;
				?>
				<article<?php if ($empresai % 2 == 0) echo ' class="right"'; ?>>
					<header>
						<div class="img thumb_emp" style="<?php if ($empresa_capa) echo 'background: #fff' ?>">
							<?php
							echo Check::image('../uploads/' . $empresa_capa, $empresa_title, 120, 70);
							?>

						</div>
						<hgroup>
							<h1><a target="_blank" href="../empresa/<?= $empresa_name; ?>" title="Ver Post"><?= $empresa_title; ?></a></h1>
							<h2><a target="_blank" href="../filtrarcidade" title="Ver Post"><?= $empresa_cidade . '/' . $empresa_uf; ?></a></h2>
						</hgroup>
					</header>
					<ul class="info post_actions">
						<li><strong>Data:</strong> <?= date('d/m/Y H:i', strtotime($empresa_date)); ?>Hs</li>
						<li><a class="act_view" target="_blank" href="../empresa/<?= $empresa_name; ?>" title="Ver no site">Ver no site</a></li>
						<li><a class="act_edit" href="painel.php?exe=empresas/update&empresaid=<?= $empresa_id; ?>" title="Editar">Editar</a></li>

						<?php if ($empresa_status == 0) { ?>
							<li><a class="act_inative" href="painel.php?exe=empresas/index&empresa=<?= $empresa_id; ?>&action=active" title="Ativar">Ativar</a></li>
						<?php } elseif ($empresa_status == 1) { ?>
							<li><a class="act_ative" href="painel.php?exe=empresas/index&empresa=<?= $empresa_id; ?>&action=inative" title="Inativar">Inativar</a></li>
						<?php } elseif ($empresa_status == 2) { ?>
							<li><a class="act_inative" href="painel.php?exe=empresas/moderacao&empresa=<?= $empresa_id; ?>&action=active" title="Ativar">Ativar</a></li>
						<?php } ?>

						<li><a class="act_delete" href="painel.php?exe=empresas/index&empresa=<?= $empresa_id; ?>&action=delete" title="Excluir">Deletar</a></li>
					</ul>

				</article>
				<?php
			}//foreach
		} else {
			$pager->returnPage();
			WSErro("Não existem empresas aguardando moderação!", WS_INFOR);
		}//if
		?>

        <div class="clear"></div>
    </section>

	<?php
	$pager->exePaginator('app_empresas');
	echo $pager->getPaginator();
	?>

    <div class="clear"></div>
</div> <!-- content home -->