
<div class = "content form_create">

	<article>

		<header>
			<h1>Atualiza Empresa:</h1>
		</header>

		<?php
		//recupero os dados do  formulário, via requisição
		$data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
		$empresaId = filter_input(INPUT_GET, 'empresaid', FILTER_VALIDATE_INT);

		if (isset($data) && $data['SendPostForm']) {
			$data['empresa_status'] = ($data['SendPostForm'] == 'Atualizar' ? '0' : '1');
			$data['empresa_capa'] = ($_FILES['empresa_capa']['tmp_name'] ? $_FILES['empresa_capa'] : 'null');
			unset($data['SendPostForm']);

			require ('_models/AdminEmpresa.class.php');
			$updateEmpresa = new AdminEmpresa();
			$updateEmpresa->exeUpdate($empresaId, $data);

			WSErro($updateEmpresa->getError()[0], $updateEmpresa->getError()[1]);
		} else {
			$read = new Read;
			$read->exeRead('app_empresas', 'WHERE empresa_id = :empresaid', "empresaid={$empresaId}");
			if (!$read->getResult()) {
				header('Location: painel.php?exe=empresas/index&empty=true');
			} else {
				$data = $read->getResult()[0];
			}
		}

		//verifico se a pagina de update é proveniente da create, para mostrar a mensagem.
		$checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
		if ($checkCreate && empty($updateEmpresa)) {
			WSErro("A empresa <b>{$data['empresa_title']}</b> foi cadastrada com sucesso no sistema!", WS_ACCEPT);
		}
		?>

		<form name="PostForm" action="" method="post" enctype="multipart/form-data">

			<label class="label">
				<span class="field">Logo da empresa: <sup>Exatamente 578x288px (JPG ou PNG)</sup></span>
				<input type="file" name="empresa_capa" />
			</label>

			<label class="label">
				<span class="field">Nome da Empresa:</span>
				<input type="text" name="empresa_title" value="<?php if (isset($data['empresa_title'])) echo $data['empresa_title']; ?>"/>
			</label>

			<label class="label">
				<span class="field">Ramo de atividade:</span>
				<input type="text" name="empresa_ramo" value="<?php if (isset($data['empresa_ramo'])) echo $data['empresa_ramo']; ?>"/>
			</label>

			<label class="label">
				<span class="field">Sobre a empresa:</span>
				<textarea name="empresa_sobre" rows="3" ><?php if (isset($data['empresa_sobre'])) echo $data['empresa_sobre']; ?></textarea>
			</label>

			<label class="label">
				<span class="field">Nome da rua / Número:</span>
				<input type="text" name="empresa_endereco" placeholder="Rua 1 5677" value="<?php if (isset($data['empresa_endereco'])) echo $data['empresa_endereco']; ?>" />
			</label>

			<label class="label" style=" display: inline; float: left;">
				<span class="field">Site da empresa:</span>
				<input type="text" name="empresa_site"  placeholder="http://www.nome-da-empresa.com.br"  style="width: 450px;" value="<?php if (isset($data['empresa_site'])) echo $data['empresa_site']; ?>"/>
			</label>


			<label class="label" style=" display: inline; float: right;">
				<span class="field">FanPage/facebook da empresa:</span>
				<input type="text" name="empresa_facebook"  placeholder="https://www.facebook.com/nomedapagina" style="width: 400px;" value="<?php if (isset($data['empresa_facebook'])) echo $data['empresa_facebook']; ?>"/>
			</label>

			<div class="clear"></div>

			<div class="label_line">

				<label class="label_small">
					<span class="field">Estado UF:</span>
					<select class="j_loadstate" name="empresa_uf">
						<option value="" disabled selected> Selecione o estado </option>
						<?php
						$readState = new Read;
						$readState->ExeRead("app_estados", "ORDER BY estado_nome ASC");
						foreach ($readState->getResult() as $estado):
							extract($estado);
							echo "<option ";
							if ($data['empresa_uf'] == $estado_id) {
								echo "selected=\"selected\" ";
							}
							echo"value=\"{$estado_id}\"> {$estado_uf} / {$estado_nome} </option>";
						endforeach;
						?>                        
					</select>
				</label>

				<label class="label_small">
					<span class="field">Cidade:</span>
					<select class="j_loadcity" name="empresa_cidade" disabled>
						<option value="" disabled selected> Selecione antes um estado </option>
						<?php
						echo "<option ";
						if ($data['empresa_cidade']) {
							$read = new Read;
							$read->exeRead('app_cidades', 'WHERE cidade_id = :id', "id={$data['empresa_cidade']}");
							echo "<option value=\"{$read->getResult()[0]['cidade_id']}\" selected=\"selected\" >{$read->getResult()[0]['cidade_nome']}</option>";
						}
						?>

					</select>
				</label>


				<label class="label_small">
					<span class="field">Indicação:</span> 
					<select name="empresa_categoria">
						<option value="" disabled selected> Indique a empresa como </option>
						<option value="onde-comer"> ONDE COMER </option>
						<option value="onde-ficar"> ONDE FICAR </option>
						<option value="onde-comprar"> ONDE COMPRAR </option>
						<option value="onde-se-divertir"> ONDE SE DIVERTIR </option>
						<?php
						echo "<option ";
						if ($data['empresa_categoria']) {
							echo "selected=\"selected\" ";
							$empresaCategoria = strtoupper(str_replace('-', ' ', $data['empresa_categoria']));
						}
						echo "value=\"{$data['empresa_categoria']}\">$empresaCategoria</option>";
						?>
					</select>
				</label>

			</div><!--/line-->


			<div class="gbform"></div>

			<input type="submit" class="btn blue" value="Atualizar" name="SendPostForm" />
			<input type="submit" class="btn green" value="Atualizar & Publicar" name="SendPostForm" />

		</form>

	</article>

	<div class="clear"></div>
</div> <!-- content form- -->