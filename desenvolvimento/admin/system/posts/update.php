<div class="content form_create">

    <article>

        <header>
            <h1>Atualizar Post:</h1>
        </header>

		<?php
		//recupero os dados do  formulário, via requisição
		$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
		$postId = filter_input(INPUT_GET, 'postid', FILTER_VALIDATE_INT);

		if (isset($post) && $post['SendPostForm']) {
			$post['post_status'] = ($post['SendPostForm'] == 'Atualizar' ? '0' : '1');
			$post['post_cover'] = ($_FILES['post_cover']['tmp_name'] ? $_FILES['post_cover'] : 'null');
			unset($post['SendPostForm']);

			require ('_models/AdminPost.class.php');
			$cadastraPost = new AdminPost;
			$cadastraPost->exeUpdate($postId, $post);

			WSErro($cadastraPost->getError()[0], $cadastraPost->getError()[1]);

			if (!empty($_FILES['gallery_covers']['tmp_name'])) {
				$sendGallery = new AdminPost;
				$sendGallery->gbSend($_FILES['gallery_covers'], $postId);
			}
		} else {
			$read = new Read;
			$read->exeRead('ws_posts', 'WHERE post_id = :postid', "postid={$postId}");
			if (!$read->getResult()) {
				header('Location: painel.php?exe=posts/index&empty=true');
			} else {
				$post = $read->getResult()[0];
			}
		}

		//verifico se a pagina de update é proveniente da create, para mostrar a mensagem.
		$checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
		if ($checkCreate && empty($cadastraPost)) {
			WSErro("O post <b>{$post['post_title']}</b> foi cadastrado com sucesso no sistema!", WS_ACCEPT);
		}
		?>

        <form name="PostForm" action="" method="post" enctype="multipart/form-data">

            <label class="label">
                <span class="field">Enviar Capa:</span>
                <input type="file" name="post_cover" />
            </label>

            <label class="label">
                <span class="field">Titulo:</span>
                <input type="text" name="post_title" value="<?php if (isset($post['post_title'])) echo $post['post_title']; ?>" />
            </label>

            <label class="label">
                <span class="field">Conteúdo:</span>
                <textarea class="js_editor" name="post_content" rows="10"><?php if (isset($post['post_content'])) echo htmlspecialchars($post['post_content']); ?></textarea>
            </label>

            <div class="label_line">

                <label class="label_small">
                    <span class="field">Data:</span>
                    <input type="text" class="formDate center" name="post_date" value="<?= date('d/m/Y H:i:s'); ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Categoria:</span>
                    <select name="post_category">
                        <option value=""> Selecione a categoria: </option>
						<?php
						$readSes = new Read;
						$readSes->exeRead('ws_categories', 'WHERE category_parent IS NULL ORDER BY category_title ASC');
						if ($readSes->getRowCount() > 0) {
							foreach ($readSes->getResult() as $ses) {
								echo"<option disabled=\"disable\" value=\"\"> {$ses['category_title']} </option>";
								$readCat = new Read;
								$readCat->exeRead('ws_categories', 'WHERE category_parent = :parent ORDER BY category_title ASC', "parent={$ses['category_id']}");
								if ($readCat->getRowCount() > 0) {
									foreach ($readCat->getResult() as $cat) {
										echo "<option ";
										if ($post['post_category'] == $cat['category_id']) {
											echo "selected=\"selected\" ";
										}
										echo "value=\"{$cat['category_id']}\"> &raquo;&raquo;{$cat['category_title']} </option>";
									}
								}
							}
						}
						?>
                    </select>
                </label>

                <label class="label_small">
                    <span class="field">Author:</span>
                    <select name="post_author">
                        <option value="<?= $_SESSION['userLogin']['user_id']; ?>"> <?= "{$_SESSION['userLogin']['user_name']} {$_SESSION['userLogin']['user_lastname']}"; ?> </option>
						<?php
						$readAuthor = new Read;
						$readAuthor->exeRead('ws_users', 'WHERE user_id != :id AND user_level >= :level ORDER BY user_name ASC', "id={$_SESSION['userLogin']['user_id']}&level=2");
						if ($readAuthor->getRowCount() > 0) {
							foreach ($readAuthor->getResult() as $aut) {
								echo"<option ";

								if ($post['post_author'] == $aut['user_id']) {
									echo "selected=\"selected\" ";
								}

								echo"value=\"{$aut['user_id']}\"> {$aut['user_name']} {$aut['user_lastname']} </option>";
							}
						}
						?>
                    </select>
                </label>

            </div><!--/line-->

            <div class="label gbform" id="gbfoco">

                <label class="label">             
                    <span class="field">Enviar Galeria:</span>
                    <input type="file" multiple name="gallery_covers[]" />
                </label>

				<?php
				$delGb = filter_input(INPUT_GET, 'gbdel', FILTER_VALIDATE_INT);
				if ($delGb) {
					require_once ('_models/AdminPost.class.php');
					$delGallery = new AdminPost;
					$delGallery->gbRemove($delGb);
					WSErro($delGallery->getError()[0], $delGallery->getError()[1]);
				}
				?>

                <ul class="gallery">
					<?php
					$gbi = 0;
					$gallery = new Read;
					$gallery->exeRead('ws_posts_gallery', 'WHERE post_id = :post', "post={$postId}");

					if ($gallery->getResult()) {
						foreach ($gallery->getResult() as $gb) {
							$gbi++;
							?>

							<li<?php if ($gbi % 5 == 0) echo ' class="right"'; ?>>
								<div class="img thumb_small">
									<!--146x70-->
									<?= Check::image('../uploads/' . $gb['gallery_image'], $gbi, 146, 100); ?>
								</div>
								<a href="painel.php?exe=posts/update&postid=<?= $postId; ?>&gbdel=<?= $gb['gallery_id']; ?>#gbfoco" class="del">Deletar</a>	
							</li>

							<?php
						}//foreach
					}//if
					?>
                </ul>                
            </div>


            <input type="submit" class="btn blue" value="Atualizar" name="SendPostForm" />
            <input type="submit" class="btn green" value="Atualizar & Publicar" name="SendPostForm" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->