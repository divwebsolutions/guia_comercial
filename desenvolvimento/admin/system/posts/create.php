<div class="content form_create">

    <article>

        <header>
            <h1>Criar Post:</h1>
        </header>

		<?php
		//recupero os dados do  formulário, via requisição
		$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
		if (isset($post) && $post['SendPostForm']) {
			$post['post_status'] = ($post['SendPostForm'] == 'Cadastrar' ? '0' : '1');
			$post['post_cover'] = ($_FILES['post_cover']['tmp_name'] ? $_FILES['post_cover'] : null);
			unset($post['SendPostForm']);

			require ('_models/AdminPost.class.php');
			$cadastraPost = new AdminPost;
			$cadastraPost->exeCreate($post);

			if ($cadastraPost->getResult()) {
				if (!empty($_FILES['gallery_covers']['tmp_name'])) {
					$sendGallery = new AdminPost;
					$sendGallery->gbSend($_FILES['gallery_covers'], $cadastraPost->getResult());
				}

				header('Location:  painel.php?exe=posts/update&create=true&postid=' . $cadastraPost->getResult());
			} else {
				WSErro($cadastraPost->getError()[0], $cadastraPost->getError()[1]);
			}
		}
		?>

        <form name="PostForm" action="" method="post" enctype="multipart/form-data">

            <label class="label">
                <span class="field">Enviar Capa:</span>
                <input type="file" name="post_cover" />
            </label>

            <label class="label">
                <span class="field">Titulo:</span>
                <input type="text" name="post_title" value="<?php if (isset($post['post_title'])) echo $post['post_title']; ?>" />
            </label>

            <label class="label">
                <span class="field">Conteúdo:</span>
                <textarea class="js_editor" name="post_content" rows="10"><?php if (isset($post['post_content'])) echo htmlspecialchars($post['post_content']); ?></textarea>
            </label>

            <div class="label_line">

                <label class="label_small">
                    <span class="field">Data:</span>
                    <input type="text" class="formDate center" name="post_date" value="<?= date('d/m/Y H:i:s'); ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Categoria:</span>
                    <select name="post_category">
                        <option value=""> Selecione a categoria: </option>
						<?php
						$readSes = new Read;
						$readSes->exeRead('ws_categories', 'WHERE category_parent IS NULL ORDER BY category_title ASC');
						if ($readSes->getRowCount() > 0) {
							foreach ($readSes->getResult() as $ses) {
								echo"<option disabled=\"disable\" value=\"\"> {$ses['category_title']} </option>";
								$readCat = new Read;
								$readCat->exeRead('ws_categories', 'WHERE category_parent = :parent ORDER BY category_title ASC', "parent={$ses['category_id']}");
								if ($readCat->getRowCount() > 0) {
									foreach ($readCat->getResult() as $cat) {
										echo "<option ";
										if ($post['post_category'] == $cat['category_id']) {
											echo "selected=\"selected\" ";
										}
										echo "value=\"{$cat['category_id']}\"> &raquo;&raquo;{$cat['category_title']} </option>";
									}
								}
							}
						}
						?>
                    </select>
                </label>

                <label class="label_small">
                    <span class="field">Author:</span>
                    <select name="post_author">
                        <option value="<?= $_SESSION['userLogin']['user_id']; ?>"> <?= "{$_SESSION['userLogin']['user_name']} {$_SESSION['userLogin']['user_lastname']}"; ?> </option>
						<?php
						$readAuthor = new Read;
						$readAuthor->exeRead('ws_users', 'WHERE user_id != :id AND user_level >= :level ORDER BY user_name ASC', "id={$_SESSION['userLogin']['user_id']}&level=2");
						if ($readAuthor->getRowCount() > 0) {
							foreach ($readAuthor->getResult() as $aut) {
								echo"<option ";

								if ($post['post_author'] == $aut['user_id']) {
									echo "selected=\"selected\" ";
								}

								echo"value=\"{$aut['user_id']}\"> {$aut['user_name']} {$aut['user_lastname']} </option>";
							}
						}
						?>
                    </select>
                </label>

            </div><!--/line-->

            <div class="label gbform">

                <label class="label">             
                    <span class="field">Enviar Galeria:</span>
                    <input type="file" multiple name="gallery_covers[]" />
                </label>                
            </div>


            <input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />
            <input type="submit" class="btn green" value="Cadastrar & Publicar" name="SendPostForm" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->