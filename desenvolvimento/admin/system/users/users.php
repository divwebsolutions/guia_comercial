<div class="content form_create">

    <article>

		<?php
		$empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
		if ($empty) {
			WSErro("Você tentou editar um usuário que não existe no sistema!", WS_INFOR);
		}
		
		$userId = filter_input(INPUT_GET, 'delete', FILTER_VALIDATE_INT);
		if ($userId) {
			require ('_models/AdminUsers.class.php');
			$deletar = new AdminUsers;
			$deletar->exeDelete($userId);
			WSErro($deletar->getError()[0], $deletar->getError()[1]);
		}


		//Pesquiso os usuários cadastrados
		$readUsers = new Read;
		$readUsers->exeRead('ws_users', "ORDER BY user_level DESC, user_name ASC");

		if (!$readUsers->getResult()) {
			WSErro("Nao existem usuários cadastrados!", WS_INFOR);
		} else {
			?>

			<h1>Usuários: <a href="painel.php?exe=users/create" title="Cadastrar Novo" class="user_cad">Cadastrar Usuário</a></h1>

			<ul class="ultable">
				<li class="t_title">
					<span class="ui center">Res:</span>
					<span class="un">Nome:</span>
					<span class="ue">E-mail:</span>
					<span class="ur center">Registro:</span>
					<span class="ua center">Atualização:</span>
					<span class="ul center">Nível:</span>
					<span class="ed center">-</span>
				</li>

				<?php
				$useri = 0;

				foreach ($readUsers->getResult() as $user) {
					extract($user);
					$useri++;
					?>            
					<li>
						<span class="ui center"><?= $useri; ?></span>
						<span class="un"><?= $user_name . ' ' . $user_lastname; ?></span>
						<span class="ue"><?= $user_email; ?></span>
						<span class="ur center"><?= date('d/m/Y H:i', strtotime($user_registration)); ?>Hs</span>
						<span class="ua center"><?= ($user_lastupdate ? date('d/m/Y H:i', strtotime($user_lastupdate)).'Hs' : '-'); ?></span>
						<span class="ul center">
							<?php
							if ($user_level == 1) {
								echo "User";
							} elseif ($user_level == 2) {
								echo "Editor";
							} elseif ($user_level == 3) {
								echo"Admin";
							}
							?>
						</span>
						<span class="ed center">
							<a href="painel.php?exe=users/update&userid=<?= $user_id ?>" class="action user_edit">Editar</a>
							<a href="painel.php?exe=users/users&delete=<?= $user_id; ?>" title="Deletar" class="action user_dele">Deletar</a>
						</span>
					</li>
					<?php
				}//foreach
			}//if
			?>

        </ul>


    </article>

    <div class="clear"></div>
</div> <!-- content home -->