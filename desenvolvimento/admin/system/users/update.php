<div class="content form_create">

    <article>

		<?php
		//Recupero dados enviados pelo formulário.
		$ClienteData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
		$userId = filter_input(INPUT_GET, 'userid', FILTER_VALIDATE_INT);

		if (!empty($ClienteData['UserCreate'])) {
			//O form foi submetido
			unset($ClienteData['UserCreate']);

			//verifico se tenho que atualizar a senha
			if (empty($ClienteData['user_password'])) {
				$ClienteData['user_password'] = 'vazio';
			}

			//manualmente por segurança
			require ('_models/AdminUsers.class.php');
			$cadastra = new AdminUsers;
			$cadastra->exeUpdate($userId, $ClienteData);
			WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
		} else {
			$read = new Read;
			$read->exeRead('ws_users', 'WHERE user_id = :id', "id={$userId}");

			if (!$read->getResult()) {
				header('Location: painel.php?exe=users/users&empty=true');
			} else {
				$ClienteData = $read->getResult()[0];
			}
		}


		//verifico se a pagina de update é proveniente da create, para mostrar a mensagem.
		$checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
		if ($checkCreate && empty($cadastra)) {
			WSErro("O usuário foi cadastrado com sucesso!", WS_ACCEPT);
		}
		?>
        <h1>Atualizar Usuário:</h1>


		<?php ?>

        <form action = "" method = "post" name = "UserCreateForm">

            <label class="label">
                <span class="field">Nome:</span>
                <input
                    type = "text"
                    name = "user_name"
                    value="<?php if (!empty($ClienteData['user_name'])) echo $ClienteData['user_name']; ?>"
                    title = "Informe seu primeiro nome"
                    required
                    />
            </label>

            <label class="label">
                <span class="field">Sobrenome:</span>
                <input
                    type = "text"
                    name = "user_lastname"
                    value="<?php if (!empty($ClienteData['user_lastname'])) echo $ClienteData['user_lastname']; ?>"
                    title = "Informe seu sobrenome"
                    required
                    />
            </label>

            <label class="label">
                <span class="field">E-mail:</span>
                <input
                    type = "email"
                    name = "user_email"
                    value="<?php if (!empty($ClienteData['user_email'])) echo $ClienteData['user_email']; ?>"
                    title = "Informe seu e-mail"
                    required
                    />
            </label>

            <div class="label_line">

                <label class="label_medium">
                    <span class="field">Senha:</span>
                    <input
                        type = "password"
                        name = "user_password"
                        value=""
						placeholder="Informe a nova senha"
                        title = "Informe sua senha [ de 6 a 12 caracteres! ]"
                        pattern = ".{6,12}"
                        />
                </label>


                <label class="label_medium">
                    <span class="field">Nível:</span>
                    <select name = "user_level" title = "Selecione o nível de usuário" required >
                        <option value = "null" >Selecione o Nível</option>
                        <option value = "1" <?php if (isset($ClienteData['user_level']) && $ClienteData['user_level'] == 1) echo 'selected="selected"'; ?>>User</option>
                        <option value="2" <?php if (isset($ClienteData['user_level']) && $ClienteData['user_level'] == 2) echo 'selected="selected"'; ?>>Editor</option>
                        <option value="3" <?php if (isset($ClienteData['user_level']) && $ClienteData['user_level'] == 3) echo 'selected="selected"'; ?>>Admin</option>
                    </select>
                </label>

            </div>

            <input type="submit" name="UserCreate" value="Atualizar Usuário" class="btn blue" />

        </form>


    </article>

    <div class="clear"></div>
</div> <!-- content home -->