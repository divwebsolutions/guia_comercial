<div class="content home">

    <aside>
        <h1 class="boxtitle">Estatísticas de Acesso:</h1>

        <article class="sitecontent boxaside">
            <h1 class="boxsubtitle">Conteúdo:</h1>

			<?php
			//OBJETO READ 
			$read = new Read;

			//VISITAS DO SITE
			$read->fullRead('SELECT SUM(siteviews_views) AS views FROM ws_siteviews');
			$views = $read->getResult()[0]['views'];

			//USUÁRIOS
			$read->fullRead('SELECT SUM(siteviews_users) AS users FROM ws_siteviews');
			$users = $read->getResult()[0]['users'];

			//MÉDIA DE PAGEVIEWS
			$read->fullRead('SELECT SUM(siteviews_pages) AS pages FROM ws_siteviews');
			if ($users > 0) {
				$resPages = $read->getResult()[0]['pages'];
				$pages = substr($resPages / $users, 0, 5);
			} else {
				$pages = 0;
			}

			//POSTS
			$read->exeRead('ws_posts');
			$posts = $read->getRowCount();

			//EMPRESAS
			$read->exeRead('app_empresas');
			$empresas = $read->getRowCount();

			//EMPRESAS AGUARDANDO MODERAÇÃO
			$read->exeRead('app_empresas', 'WHERE empresa_status = 2');
			$empresasMod = $read->getRowCount();
			?>



            <ul>
                <li class="view"><span><?= $views; ?></span> visitas</li>
                <li class="user"><span><?= $users; ?></span> usuários</li>
                <li class="page"><span><?= $pages; ?></span> pageviews</li>
                <li class="line"></li>
                <li class="post"><span><?= $posts; ?></span> posts</li>
                <li class="emp"><span><?= $empresas; ?></span> empresas (<?= $empresasMod; ?> Aguardando moderação)</li>
                <!--<li class="comm"><span></span> comentários</li>-->
            </ul>
            <div class="clear"></div>
        </article>

        <article class="useragent boxaside">
            <h1 class="boxsubtitle">Navegador:</h1>

			<?php
			//LÊ O TOTAL DE VISITAS DOS NAVEGADORES
			$read->fullRead('SELECT SUM(agent_views) AS totalViews FROM ws_siteviews_agent');
			$totalViews = $read->getResult()[0]['totalViews'];

			$read->exeRead('ws_siteviews_agent', 'ORDER BY agent_views DESC LIMIT 3');
			if (!$read->getResult()) {
				WSErro("Opss, ainda não existem estatísticas de navegadores!", WS_INFOR);
			} else {
				echo"<ul>";
				foreach ($read->getResult() as $nav) {
					extract($nav);

					//REALIZA PORCENTAGEM DE VISITAS POR NAVEGADOR
					$percent = substr(( $agent_views / $totalViews ) * 100, 0, 5);
					?>

					<li>
						<p><strong><?= $agent_name; ?>:</strong> <?= $percent; ?>%</p>
						<span style="width: <?= $percent; ?>%"></span>
						<p><?= $agent_views; ?> visitas</p>
					</li>

					<?php
				}//foreach
				echo"</ul>";
			}//if
			?>


            <div class="clear"></div>
        </article>
    </aside>

    <section class="content_statistics">

        <h1 class="boxtitle">Publicações:</h1>

        <section>
            <h1 class="boxsubtitle">Artigos Recentes:</h1>

			<?php
			$i = 0;
			$readArtigos = new Read;
			$readArtigos->exeRead('ws_posts', 'ORDER BY post_date DESC LIMIT 3');
			if ($readArtigos->getResult()) {
				foreach ($readArtigos->getResult() as $artigo) {
					extract($artigo);
					$i++;
					?>
					<article<?php if ($i % 2 == 0) echo ' class="right"'; ?>>

						<div class="img thumb_small" style="<?php if ($post_cover) echo 'background: #fff' ?>">
							<?= Check::image('../uploads/' . $post_cover, $post_title, 120, 70); ?>
						</div>
						<h1><a target="_blank" href="../artigo/<?= $post_name; ?>" title="Ver Post"><?= $post_title; ?></a></h1>
						<ul class="info post_actions">
							<li><strong>Data:</strong> <?= date('d/m/Y H:i', strtotime($post_date)); ?>Hs</li>
							<li><a class="act_view" target="_blank" href="../artigo/<?= $post_name; ?>" title="Ver no site">Ver no site</a></li>
							<li><a class="act_edit" href="painel.php?exe=posts/update&postid=<?= $post_id; ?>" title="Editar">Editar</a></li>

							<?php if (!$post_status) { ?>
								<li><a class="act_inative" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=active" title="Ativar">Ativar</a></li>
							<?php } else { ?>
								<li><a class="act_ative" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=inative" title="Inativar">Inativar</a></li>
							<?php } ?>

							<li><a class="act_delete" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=delete" title="Excluir">Deletar</a></li>
						</ul>

					</article>
					<?php
				}//foreach
			}//if
			?>
        </section>          


        <section>
            <h1 class="boxsubtitle">Artigos Mais Vistos:</h1>

			<?php
			$i = 0;
			$readArtigos = new Read;
			$readArtigos->exeRead('ws_posts', 'ORDER BY post_views DESC LIMIT 3');
			if ($readArtigos->getResult()) {
				foreach ($readArtigos->getResult() as $artigo) {
					extract($artigo);
					$i++;
					?>

					<article<?php if ($i % 2 == 0) echo ' class="right"'; ?>>

						<div class="img thumb_small" style="<?php if ($post_cover) echo 'background: #fff' ?>">
							<?= Check::image('../uploads/' . $post_cover, $post_title, 120, 70); ?>
						</div>
						<h1><a target="_blank" href="../artigo/<?= $post_name; ?>" title="Ver Post"><?= $post_title; ?></a></h1>
						<ul class="info post_actions">
							<li><strong>Data:</strong> <?= date('d/m/Y H:i', strtotime($post_date)); ?>Hs</li>
							<li><a class="act_view" target="_blank" href="../artigo/<?= $post_name; ?>" title="Ver no site">Ver no site</a></li>
							<li><a class="act_edit" href="painel.php?exe=posts/update&postid=<?= $post_id; ?>" title="Editar">Editar</a></li>

							<?php if (!$post_status) { ?>
								<li><a class="act_inative" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=active" title="Ativar">Ativar</a></li>
							<?php } else { ?>
								<li><a class="act_ative" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=inative" title="Inativar">Inativar</a></li>
							<?php } ?>

							<li><a class="act_delete" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=delete" title="Excluir">Deletar</a></li>
						</ul>

					</article>
					<?php
				}//foreach
			}//if
			?>
        </section>                           

    </section> <!-- Estatísticas -->

    <div class="clear"></div>
</div> <!-- content home -->