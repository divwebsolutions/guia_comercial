<?php
$view = (!empty($view) ? $view : $view = new View);
$post_id = (!empty($post_id) ? $post_id : null );

$side = new Read;
$tpl_p = $view->load('article_p');
?>

<aside class="main-sidebar">
    <article class="ads">
        <header>
            <h1>Anúncio Patrocinado:</h1>
            <a href="#contato" title="Cidade Online - Anúncie seu negócio aqui!">
                <img src="<?= INCLUDE_PATH; ?>/_tmp/banner_large.png" alt="Cidade Online - Anúncie seu negócio aqui!" title="Cidade Online - Anúncie seu negócio aqui!" />
            </a>
        </header>
    </article>

    <section class="widget art-list last-publish">
        <h2 class="line_title"><span class="oliva">Últimas Atualizações:</span></h2>

		<?php
		$side->exeRead('ws_posts', 'WHERE post_status = 1 AND post_id != :side ORDER BY post_date DESC LIMIT 3', "side={$post_id}");
		if ($side->getResult()) {
			foreach ($side->getResult() as $last) {
				$last['datetime'] = date('Y-m-d', strtotime($last['post_date']));
				$last['pubdate'] = date('d/m/Y H:i', strtotime($last['post_date']));
				$view->show($last, $tpl_p);
			}//ENDFOREACH;
		}//ENDIF;
		?>
    </section>

    <section class="widget art-list most-view">
        <h2 class="line_title"><span class="vermelho">Destaques:</span></h2>

		<?php
		$side->exeRead('ws_posts', 'WHERE post_status = 1 AND post_id != :side ORDER BY post_views DESC LIMIT 3', "side={$post_id}");
		if ($side->getResult()) {
			foreach ($side->getResult() as $topNews) {
				$topNews['datetime'] = date('Y-m-d', strtotime($topNews['post_date']));
				$topNews['pubdate'] = date('d/m/Y H:i', strtotime($topNews['post_date']));
				$view->show($topNews, $tpl_p);
			}//ENDFOREACH;
		}//ENDIF;
		?>

    </section>
</aside>