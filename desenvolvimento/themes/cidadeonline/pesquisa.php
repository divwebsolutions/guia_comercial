<?php
$search = $link->getLocal()[1];
$count = ($link->getData()['count'] ? $link->getData()['count'] : '0');
?>	

<!--HOME CONTENT-->
<div class="site-container">

    <section class="page_categorias">
        <header class="cat_header">
            <h2>Pesquisa por: <strong><?= $search; ?></strong></h2>


            <p class="tagline">Sua pesquisa por <i><?= $search; ?></i> retornou <?= $count; ?> resultados!</p>
        </header>


		<?php
		$getPage = (!empty($link->getLocal()[2]) ? $link->getLocal()[2] : 1);
		$pager = new Pager(HOME . '/pesquisa/' . $search . '/');
		$pager->exePager($getPage, 4);

		$readArt = new Read;
		$readArt->exeRead('ws_posts', "WHERE post_status = 1 AND (post_title LIKE '%' :link '%' OR post_content LIKE '%' :link '%') ORDER BY post_date DESC LIMIT :limit OFFSET :offset", "link={$search}&limit={$pager->getLimit()}&offset={$pager->getOffset()}");
		if (!$readArt->getResult()) {
			$pager->returnPage();
			WSErro("Desculpe, sua pesquisa não retornou resultados. Você pode resumir sua pesquisa, ou tentar outros termos!", WS_INFOR);
		} else {
			$cc = 0;
			$view = new View;
			$tpl_art = $view->load('article_m');
			foreach ($readArt->getResult() as $art) {
				$cc++;
				$class = ($cc % 3 == 0 ? ' class="right"' : null);
				echo "<span{$class}>";
				$art['post_title'] = Check::limitaString($art['post_title'], 9);
				$art['post_content'] = Check::limitaString($art['post_content'], 20);
				$art['datetime'] = date('Y-m-d', strtotime($art['post_date']));
				$art['pubdate'] = date('d/m/Y H:i', strtotime($art['post_date']));
				$view->show($art, $tpl_art);
				echo "</span>";
			}//ENDFOREACH;
		}//ENDIF;

		echo '<nav class="paginator">';
		echo '<h2>Mais resultados para NOME DA CATEGORIA</h2>';
		$pager->exePaginator('ws_posts', "WHERE post_status = 1 AND (post_title LIKE '%' :link '%' OR post_content LIKE '%' :link '%')", "link={$search}");
		echo $pager->getPaginator();
		echo '</nav>';
		?>
    </section>

    <div class="clear"></div>
</div><!--/ site container -->