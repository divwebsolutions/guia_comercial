<!--HOME CONTENT-->
<?php
$empLink = $link->getData()['empresa_link'];
$cat = $link->getData()['empresa_cat'];
?>

<div class="site-container">

    <section class="page_empresas">
        <header class="emp_header">
            <h2><?= $cat; ?></h2>
            <p class="tagline">Conheça as empresas cadastradas no seu guia online. Encontre aqui empresas <?= $cat; ?></p>
        </header>

		<?php
		$getPage = (!empty($link->getLocal()[2]) ? $link->getLocal()[2] : 1);
		$pager = new Pager(HOME . '/empresas/' . $empLink . '/');
		$pager->exePager($getPage, 6);

		$readEmp = new Read;
		$readEmp->exeRead('app_empresas', 'WHERE empresa_status = 1 AND empresa_categoria = :cat ORDER BY empresa_date DESC LIMIT :limit '
				. 'OFFSET :offset', "cat={$empLink}&limit={$pager->getLimit()}&offset={$pager->getOffset()}");
		if (!$readEmp->getResult()) {
			$pager->returnPage();
			WSErro("Desculpe, ainda não existem empresas cadastradas {$cat}, por favor volte depois!", WS_INFOR);
		} else {
			$view = new View;
			$tpl = $view->load('empresa_list');
			foreach ($readEmp->getResult() as $emp) {
				$cidade = new Read;
				$cidade->exeRead('app_cidades', 'WHERE cidade_id = :cidadeid', "cidadeid={$emp['empresa_cidade']}");
				$cidade = $cidade->getResult()[0]['cidade_nome'];

				$estado = new Read;
				$estado->exeRead('app_estados', 'WHERE estado_id = :estadoid', "estadoid={$emp['empresa_uf']}");
				$estado = $estado->getResult()[0]['estado_uf'];

				$emp['empresa_cidade'] = $cidade;
				$emp['empresa_uf'] = $estado;

				$view->show($emp, $tpl);
			}

			echo '<footer>';
			echo '<nav class="paginator">';
			echo '<h2>Mais resultados para NOME DA CATEGORIA</h2>';

			$pager->exePaginator('app_empresas', 'WHERE empresa_status = 1 AND empresa_categoria = :cat', "cat={$empLink}");
			echo $pager->getPaginator();
			
			echo '</nav>';
			echo '</footer>';
		}
		?>
    </section>

    <!--SIDEBAR-->
	<?php require(REQUIRE_PATH . '/inc/sidebar.inc.php'); ?>

    <div class="clear"></div>
</div><!--/ site container -->