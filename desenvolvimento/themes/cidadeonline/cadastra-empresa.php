<!--HOME CONTENT-->
<!--SCRIPT PARA LER ESTADOS E CIDADES-->
<script type="text/javascript">
    window.onload = function () {
	new dgCidadesEstados(document.getElementById('estado'), document.getElementById('cidade'), true);
    }
</script>
<!--/SRICPT ESTADO/CIDADE-->
<div class="site-container" id="c">

    <article class="cadastra_empresa">
		<header class="cat_header">
			<h1>Cadastre sua empresa</h1>
			<p class="tagline">Olá, este é um modelo de cadastro de empresa para que você possa realizar uma simples interação com o site. 
				Informe os dados e vou moderar e aprovar caso estes estejam corretos!</p>
		</header>

		<?php
		if (isset($link->getLocal()[1]) && $link->getLocal()[1] == 'create') {
			WSErro("Tudo certo meu querido! Sua empresa foi cadastrada com sucesso.", WS_ACCEPT);
			WSErro("<b>Moderação:</b> O cadastro é moderado, logo esse só vai para o site depois da ativação pelo ADMIN!", WS_ALERT);
		}

		$postEmpresa = filter_input(INPUT_POST, 'SendPostForm', FILTER_DEFAULT);
		if ($postEmpresa) {
			$empDataSet = filter_input_array(INPUT_POST, FILTER_DEFAULT);
			unset($empDataSet['SendPostForm']);


			$empDataGet = array_map('strip_tags', $empDataSet);
			$data = array_map('trim', $empDataGet);
			$data['empresa_capa'] = $_FILES['empresa_capa'];

			//VERIFICO SE TEM CAMPOS NÃO PREENCHIDOS
			if (in_array('', $data) || $data['empresa_capa']['error'] != 0) {
				WSErro("Oppsss: Para cadastrar sua empresa, preencha todos os campos. <b>Tudo é obrigatório aqui!</b>", WS_ALERT);
			} else {

				//VERIFICO ID DO ESTADO
				$readEstado = new Read;
				$readEstado->exeRead('app_estados', 'WHERE estado_uf = :uf', "uf={$data['empresa_uf']}");
				if (!$readEstado->getResult()) {
					WSErro("Desculpe, ocorreu um erro ao selecionar o estado, entre em contato com o admin!", WS_ALERT);
				} else {
					$data['empresa_uf'] = $readEstado->getResult()[0]['estado_id'];
				}

				//VERIFICO ID DA CIDADE
				$readCidade = new Read;
				$readCidade->exeRead('app_cidades', 'WHERE cidade_nome = :cidade', "cidade={$data['empresa_cidade']}");
				if (!$readCidade->getResult()) {
					WSErro("Desculpe, ocorreu um erro ao selecionar a cidade, entre em contato com o admin!", WS_ALERT);
				} else {
					$data['empresa_cidade'] = $readCidade->getResult()[0]['cidade_id'];
				}

				//CADASTRAR EMPRESA...NOME DA CAPA COM EMPRESA_NAME
				$data['empresa_name'] = Check::validaNome($data['empresa_title']);
				$data['empresa_date'] = Check::data('Y-m-d H:i:s');

				$checkName = new Read;
				$checkName->exeRead('app_empresas', 'WHERE empresa_title = :title', "title={$data['empresa_title']}");
				if ($checkName->getResult()) {
					$data['empresa_name'] = $data['empresa_name'] . '-' . $checkName->getRowCount();
				}//ENDIF

				$sendCapa = new Upload('uploads/');
				$sendCapa->image($data['empresa_capa'], $data['empresa_name'], 'empresas', 578);
				$data['empresa_capa'] = $sendCapa->getResult();

				$data['empresa_status'] = '2';

				$cadastra = new Create;
				$cadastra->exeCreate("app_empresas", $data);
				if ($cadastra->getResult()) {
					header('Location: ' . HOME . '/cadastra-empresa/create');
				}//ENDIF
			}//ENDIF
		}//ENDIF
		?>

		<!-- FORMULÁRIO DE CADASTRO-->
		<form name="PostForm" action="" method="post" enctype="multipart/form-data">

			<label class="label">
				<span class="field">Logo da empresa: <sup>Exatamente 578x288px (JPG ou PNG)</sup></span>
				<input type="file" name="empresa_capa" />
			</label>

			<label class="label">
				<span class="field">Nome da Empresa:</span>
				<input type="text" name="empresa_title" value="<?php if (isset($empDataSet['empresa_title'])) echo $empDataSet['empresa_title']; ?>"/>
			</label>

			<label class="label">
				<span class="field">Ramo de atividade:</span>
				<input type="text" name="empresa_ramo" value="<?php if (isset($empDataSet['empresa_ramo'])) echo $empDataSet['empresa_ramo']; ?>"/>
			</label>

			<label class="label">
				<span class="field">Sobre a empresa:</span>
				<textarea name="empresa_sobre" rows="3" ><?php if (isset($empDataSet['empresa_sobre'])) echo $empDataSet['empresa_sobre']; ?></textarea>
			</label>

			<label class="label">
				<span class="field">Nome da rua / Número:</span>
				<input type="text" name="empresa_endereco" placeholder="Rua 1 5677" value="<?php if (isset($empDataSet['empresa_endereco'])) echo $empDataSet['empresa_endereco']; ?>" />
			</label>

			<label class="label" style=" display: inline; float: left;">
				<span class="field">Site da empresa:</span>
				<input type="url" name="empresa_site"  placeholder="http://www.nome-da-empresa.com.br"  value="<?php if (isset($empDataSet['empresa_site'])) echo $empDataSet['empresa_site']; ?>"/>
			</label>


			<label class="label" style=" display: inline; float: right;">
				<span class="field">Facebook Page:</span>
				<input type="url" name="empresa_facebook"  placeholder="https://www.facebook.com/nomedapagina"  value="<?php if (isset($empDataSet['empresa_facebook'])) echo $empDataSet['empresa_facebook']; ?>"/>
			</label>

			<div class="clear"></div>

			<div class="label_line">

				<label class="label_small">
					<span class="field">Estado UF:</span>
					<select id="estado" name="empresa_uf"></select>
				</label>

				<label class="label_small">
					<span class="field">Cidade:</span>
					<select id="cidade" name="empresa_cidade"></select>
				</label>


				<label class="label_small">
					<span class="field">Indicação:</span> 
					<select name="empresa_categoria">
						<option value="" disabled selected> Indique a empresa como </option>
						<option value="onde-comer"> ONDE COMER </option>
						<option value="onde-ficar"> ONDE FICAR </option>
						<option value="onde-comprar"> ONDE COMPRAR </option>
						<option value="onde-se-divertir"> ONDE SE DIVERTIR </option>
					</select>
				</label>

			</div><!--/line-->


			<div class="gbform"></div>

			<input type="submit" class="btn blue" value="Enviar para análise" name="SendPostForm" />
		</form>
	</article>

    <div class="clear"></div>
</div><!--/ site container -->