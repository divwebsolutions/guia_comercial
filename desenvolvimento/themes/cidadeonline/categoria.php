<?php
if ($link->getData()) {
	extract($link->getData());
} else {
	header('Location: ' . HOME . DIRECTORY_SEPARATOR . '404');
}
?>
<!--HOME CONTENT-->
<div class="site-container">

    <section class="page_categorias">
        <header class="cat_header">
            <h2><?= $category_title ?></h2>
            <p class="tagline"><?= $category_content; ?></p>
        </header>
		<?php
		$getPage = (!empty($link->getLocal()[2]) ? $link->getLocal()[2] : 1);
		$pager = new Pager(HOME . '/categoria/' . $category_name . '/');
		$pager->exePager($getPage, 12);

		$readCat = new Read;
		$readCat->exeRead('ws_posts', 'WHERE post_status = 1 AND (post_category = :cat OR post_cat_parent = :cat) ORDER BY post_date DESC LIMIT :limit OFFSET :offset', "cat={$category_id}&limit={$pager->getLimit()}&offset={$pager->getOffset()}");
		if (!$readCat->getResult()) {
			$pager->returnPage();
			WSErro("Desculpe, não existem artigos publicados aqui, por favor volte para o ínicio!", WS_INFOR);
		} else {
			$cc = 0;
			$view = new View;
			$tpl_cat = $view->load('article_m');
			foreach ($readCat->getResult() as $cat) {
				$cc++;
				$class = ($cc % 3 == 0 ? ' class="right"' : null);
				echo "<span{$class}>";
				$cat['post_title'] = Check::limitaString($cat['post_title'], 9);
				$cat['post_content'] = Check::limitaString($cat['post_content'], 20);
				$cat['datetime'] = date('Y-m-d', strtotime($cat['post_date']));
				$cat['pubdate'] = date('d/m/Y H:i', strtotime($cat['post_date']));
				$view->show($cat, $tpl_cat);
				echo "</span>";
			}//ENDFOREACH;
		}//ENDIF;

		echo '<nav class="paginator">';
		echo '<h2>Mais resultados para NOME DA CATEGORIA</h2>';

		$pager->exePaginator('ws_posts', 'WHERE post_status = 1 AND (post_category = :cat OR post_cat_parent = :cat)', "cat={$category_id}");
		echo $pager->getPaginator();

		echo '</nav>';
		?>
    </section>

    <div class="clear"></div>
</div><!--/ site container -->