<?php
//Carrego os template_views
$view = new View;
$tpl_g = $view->load('article_g');
$tpl_m = $view->load('article_m');
$tpl_p = $view->load('article_p');
$tpl_empresa = $view->load('empresa_p');
?>

<!--HOME SLIDER-->
<section class="main-slider">
    <h3>Últimas Atualizações:</h3>


	<div class="slidecount">
		<?php
		$cat = Check::categoriaByName('noticias');
		$post = new Read;
		$post->exeRead('ws_posts', 'WHERE post_status = 1 AND (post_cat_parent = :cat OR post_category = :cat) ORDER BY post_date DESC LIMIT :limit OFFSET :offset', "cat={$cat}&limit=3&offset=0");
		if (!$post->getResult()) {
			WSErro("Desculpe, ainda não existem posts cadastrados. Por favor, volte mais tarde!", WS_INFOR);
		} else {
			foreach ($post->getResult() as $slide) {
				$slide['post_title'] = Check::limitaString($slide['post_title'], 12);
				$slide['post_content'] = Check::limitaString($slide['post_content'], 38);
				$slide['datetime'] = date('Y-m-d', strtotime($slide['post_date']));
				$slide['pubdate'] = date('d/m/Y H:i', strtotime($slide['post_date']));
				$view->show($slide, $tpl_g);
			}//ENDFOREACH;
		}//ENDIF;
		?>                
	</div>

	<div class="slidenav"></div>   
    <!-- Container Slide -->

</section><!-- /slider -->

<!--HOME CONTENT-->
<div class="site-container">

    <section class="main-destaques">
        <h2>Destaques:</h2>

        <section class="main_lastnews">
            <h1 class="line_title"><span class="oliva">Últimas Notícias:</span></h1>

			<?php
			$post->setPlaces("cat={$cat}&limit=1&offset=3");
			if (!$post->getResult()) {
				WSErro("Desculpe, não existe uma notícia destaque para ser exibida agora, mas estamos trabalhando para resolver isso, volte mais tarde!", WS_INFOR);
			} else {
				$new = $post->getResult()[0];
				$new['post_title'] = Check::limitaString($new['post_title'], 12);
				$new['post_content'] = Check::limitaString($new['post_content'], 38);
				$new['datetime'] = date('Y-m-d', strtotime($new['post_date']));
				$new['pubdate'] = date('d/m/Y H:i', strtotime($new['post_date']));
				$view->show($new, $tpl_m);
			}
			?>

            <div class="last_news">
				<?php
				$post->setPlaces("cat={$cat}&limit=4&offset=4");
				if (!$post->getResult()) {
					WSErro("Desculpe, não temos mais noticias para serem exibidas aqui. Por favor volte mais tarde!", WS_INFOR);
				} else {
					foreach ($post->getResult() as $lastNews) {
						$lastNews['post_title'] = Check::limitaString($lastNews['post_title'], 12);
						$lastNews['datetime'] = date('Y-m-d', strtotime($lastNews['post_date']));
						$lastNews['pubdate'] = date('d/m/Y H:i', strtotime($lastNews['post_date']));
						$view->show($lastNews, $tpl_p);
					}
				}//ENDIF;
				?>
            </div>


            <nav class="guia_comercial">
                <h1>Guia de Empresas:</h1>
                <ul class="navitem">
                    <li id="comer" class="azul tabactive">Onde Comer</li>
                    <li id="ficar">Onde Ficar</li>
                    <li id="comprar" class="verde">Onde Comprar</li>
                    <li id="divertir" class="roxo">Onde se Divertir</li>
                </ul>

                <div class="tab comer">
					<?php
					$empCat = 'onde-comer';
					$empresa = new Read;
					$empresa->exeRead('app_empresas', 'WHERE empresa_status = 1 AND empresa_categoria = :empcat ORDER BY rand() LIMIT 4', "empcat={$empCat}");
					if (!$empresa->getResult()) {
						WSErro("Desculpe, não existem empresas cadastradas nessa categoria ainda. Por favor, volte mais tarde!", WS_INFOR);
					} else {
						foreach ($empresa->getResult() as $emp) {
							$view->show($emp, $tpl_empresa);
						}
					}
					?>
                </div>

                <div class="tab ficar none">
					<?php
					$empCat = 'onde-ficar';
					$empresa->setPlaces("empcat={$empCat}");
					if (!$empresa->getResult()) {
						WSErro("Desculpe, não existem empresas cadastradas nessa categoria ainda. Por favor, volte mais tarde!", WS_INFOR);
					} else {
						foreach ($empresa->getResult() as $emp) {
							$view->show($emp, $tpl_empresa);
						}
					}
					?>
                </div>

                <div class="tab comprar none">
					<?php
					$empCat = 'onde-comprar';
					$empresa->setPlaces("empcat={$empCat}");
					if (!$empresa->getResult()) {
						WSErro("Desculpe, não existem empresas cadastradas nessa categoria ainda. Por favor, volte mais tarde!", WS_INFOR);
					} else {
						foreach ($empresa->getResult() as $emp) {
							$view->show($emp, $tpl_empresa);
						}
					}
					?>
                </div>

                <div class="tab divertir none">
					<?php
					$empCat = 'onde-se-divertir';
					$empresa->setPlaces("empcat={$empCat}");
					if (!$empresa->getResult()) {
						WSErro("Desculpe, não existem empresas cadastradas nessa categoria ainda. Por favor, volte mais tarde!", WS_INFOR);
					} else {
						foreach ($empresa->getResult() as $emp) {
							$view->show($emp, $tpl_empresa);
						}
					}
					?>
                </div>                        
            </nav>
        </section><!--  last news -->


        <aside>
            <div class="aside-banner">
                <!--300x250-->
                <a href="#contato" title="Cidade Online - Anúncie seu negócio aqui!">
                    <img src="<?= INCLUDE_PATH; ?>/_tmp/banner_large.png" title="Cidade Online - Anúncie seu negócio aqui!" alt="Cidade Online - Anúncie seu negócio aqui!" />
                </a>
            </div>

            <h1 class="line_title"><span class="vermelho">Destaques:</span></h1>

			<?php
			$asidePosts = new Read;
			$asidePosts->exeRead('ws_posts', 'WHERE post_status = 1 ORDER BY post_views DESC, post_date DESC LIMIT 3');
			if (!$asidePosts->getResult()) {
				WSErro("Desculpa, nenhuma notícia em destaque no momento. Por favor, volte mais tarde!", WS_INFOR);
			} else {
				foreach ($asidePosts->getResult() as $aPosts) {
					$aPosts['post_title'] = Check::limitaString($aPosts['post_title'], 12);
					$aPosts['datetime'] = date('Y-m-d', strtotime($aPosts['post_date']));
					$aPosts['pubdate'] = date('d/m/Y H:i', strtotime($aPosts['post_date']));
					$view->show($aPosts, $tpl_p);
				}
			}
			?>

        </aside>               

    </section><!-- destaques -->


    <section class="last_forcat">

        <h1>Por categoria!</h1>

        <section class="eventos">
            <h2 class="line_title"><span class="roxo">Aconteceu:</span></h2>

			<?php
			$cat = Check::categoriaByName('aconteceu');
			$post->setPlaces("cat={$cat}&limit=1&offset=0");
			if (!$post->getResult()) {
				WSErro("Desculpe, não existe uma notícia destaque para ser exibida agora, mas estamos trabalhando para resolver isso, volte mais tarde!", WS_INFOR);
			} else {
				$new = $post->getResult()[0];
				$new['post_title'] = Check::limitaString($new['post_title'], 9);
				$new['post_content'] = Check::limitaString($new['post_content'], 20);
				$new['datetime'] = date('Y-m-d', strtotime($new['post_date']));
				$new['pubdate'] = date('d/m/Y H:i', strtotime($new['post_date']));
				$view->show($new, $tpl_m);
			}
			?>

            <div class="last_news">
				<?php
				$post->setPlaces("cat={$cat}&limit=3&offset=1");
				if (!$post->getResult()) {
					WSErro("Desculpe, não temos mais noticias para serem exibidas aqui. Por favor volte mais tarde!", WS_INFOR);
				} else {
					foreach ($post->getResult() as $lastNews) {
						$lastNews['post_title'] = Check::limitaString($lastNews['post_title'], 12);
						$lastNews['datetime'] = date('Y-m-d', strtotime($lastNews['post_date']));
						$lastNews['pubdate'] = date('d/m/Y H:i', strtotime($lastNews['post_date']));
						$view->show($lastNews, $tpl_p);
					}
				}//ENDIF;
				?>
            </div>
        </section>


        <section class="esportes">
            <h2 class="line_title"><span class="verde">Eventos:</span></h2>

			<?php
			$cat = Check::categoriaByName('eventos');
			$post->setPlaces("cat={$cat}&limit=1&offset=0");
			if (!$post->getResult()) {
				WSErro("Desculpe, não existe uma notícia destaque para ser exibida agora, mas estamos trabalhando para resolver isso, volte mais tarde!", WS_INFOR);
			} else {
				$new = $post->getResult()[0];
				$new['post_title'] = Check::limitaString($new['post_title'], 9);
				$new['post_content'] = Check::limitaString($new['post_content'], 20);
				$new['datetime'] = date('Y-m-d', strtotime($new['post_date']));
				$new['pubdate'] = date('d/m/Y H:i', strtotime($new['post_date']));
				$view->show($new, $tpl_m);
			}
			?>

            <div class="last_news">
				<?php
				$post->setPlaces("cat={$cat}&limit=3&offset=1");
				if (!$post->getResult()) {
					WSErro("Desculpe, não temos mais noticias para serem exibidas aqui. Por favor volte mais tarde!", WS_INFOR);
				} else {
					foreach ($post->getResult() as $lastNews) {
						$lastNews['post_title'] = Check::limitaString($lastNews['post_title'], 12);
						$lastNews['datetime'] = date('Y-m-d', strtotime($lastNews['post_date']));
						$lastNews['pubdate'] = date('d/m/Y H:i', strtotime($lastNews['post_date']));
						$view->show($lastNews, $tpl_p);
					}
				}//ENDIF;
				?>
            </div>
        </section>


        <section class="baladas">
            <h2 class="line_title"><span class="azul">Esportes:</span></h2>

			<?php
			$cat = Check::categoriaByName('esportes');
			$post->setPlaces("cat={$cat}&limit=1&offset=0");
			if (!$post->getResult()) {
				WSErro("Desculpe, não existe uma notícia destaque para ser exibida agora, mas estamos trabalhando para resolver isso, volte mais tarde!", WS_INFOR);
			} else {
				$new = $post->getResult()[0];
				$new['post_title'] = Check::limitaString($new['post_title'], 9);
				$new['post_content'] = Check::limitaString($new['post_content'], 20);
				$new['datetime'] = date('Y-m-d', strtotime($new['post_date']));
				$new['pubdate'] = date('d/m/Y H:i', strtotime($new['post_date']));
				$view->show($new, $tpl_m);
			}
			?>

            <div class="last_news">
				<?php
				$post->setPlaces("cat={$cat}&limit=3&offset=1");
				if (!$post->getResult()) {
					WSErro("Desculpe, não temos mais noticias para serem exibidas aqui. Por favor volte mais tarde!", WS_INFOR);
				} else {
					foreach ($post->getResult() as $lastNews) {
						$lastNews['post_title'] = Check::limitaString($lastNews['post_title'], 12);
						$lastNews['datetime'] = date('Y-m-d', strtotime($lastNews['post_date']));
						$lastNews['pubdate'] = date('d/m/Y H:i', strtotime($lastNews['post_date']));
						$view->show($lastNews, $tpl_p);
					}
				}//ENDIF;
				?>
            </div>
        </section>

    </section><!-- categorias -->
    <div class="clear"></div>
</div><!--/ site container -->